Š§
*č)
9
Add
x"T
y"T
z"T"
Ttype:
2	
S
AddN
inputs"T*N
sum"T"
Nint(0"
Ttype:
2	
Ń
	ApplyAdam
var"T	
m"T	
v"T
beta1_power"T
beta2_power"T
lr"T

beta1"T

beta2"T
epsilon"T	
grad"T
out"T"
Ttype:
2	"
use_lockingbool( 
l
ArgMax

input"T
	dimension"Tidx

output	"
Ttype:
2	"
Tidxtype0:
2	
x
Assign
ref"T

value"T

output_ref"T"	
Ttype"
validate_shapebool("
use_lockingbool(
{
BiasAdd

value"T	
bias"T
output"T"
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
{
BiasAddGrad
out_backprop"T
output"T"
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
R
BroadcastGradientArgs
s0"T
s1"T
r0"T
r1"T"
Ttype0:
2	
8
Cast	
x"SrcT	
y"DstT"
SrcTtype"
DstTtype
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
É
Conv2D

input"T
filter"T
output"T"
Ttype:
2"
strides	list(int)"
use_cudnn_on_gpubool(""
paddingstring:
SAMEVALID"-
data_formatstringNHWC:
NHWCNCHW
ď
Conv2DBackpropFilter

input"T
filter_sizes
out_backprop"T
output"T"
Ttype:
2"
strides	list(int)"
use_cudnn_on_gpubool(""
paddingstring:
SAMEVALID"-
data_formatstringNHWC:
NHWCNCHW
î
Conv2DBackpropInput
input_sizes
filter"T
out_backprop"T
output"T"
Ttype:
2"
strides	list(int)"
use_cudnn_on_gpubool(""
paddingstring:
SAMEVALID"-
data_formatstringNHWC:
NHWCNCHW
A
Equal
x"T
y"T
z
"
Ttype:
2	

W

ExpandDims

input"T
dim"Tdim
output"T"	
Ttype"
Tdimtype0:
2	
4
Fill
dims

value"T
output"T"	
Ttype
+
Floor
x"T
y"T"
Ttype:
2
>
FloorDiv
x"T
y"T
z"T"
Ttype:
2	
S
HistogramSummary
tag
values"T
summary"
Ttype0:
2		
.
Identity

input"T
output"T"	
Ttype
<
L2Loss
t"T
output"T"
Ttype:
2	
o
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2
ž
MaxPool

input"T
output"T"
Ttype0:
2"
ksize	list(int)(0"
strides	list(int)(0""
paddingstring:
SAMEVALID"-
data_formatstringNHWC:
NHWCNCHW
ä
MaxPoolGrad

orig_input"T
orig_output"T	
grad"T
output"T"
ksize	list(int)(0"
strides	list(int)(0""
paddingstring:
SAMEVALID"-
data_formatstringNHWC:
NHWCNCHW"
Ttype0:
2
:
Maximum
x"T
y"T
z"T"
Ttype:	
2	

Mean

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( "
Ttype:
2	"
Tidxtype0:
2	
8
MergeSummary
inputs*N
summary"
Nint(0
b
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(
<
Mul
x"T
y"T
z"T"
Ttype:
2	
-
Neg
x"T
y"T"
Ttype:
	2	

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
A
Placeholder
output"dtype"
dtypetype"
shapeshape: 

Prod

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( "
Ttype:
2	"
Tidxtype0:
2	
}
RandomUniform

shape"T
output"dtype"
seedint "
seed2int "
dtypetype:
2"
Ttype:
2	
=
RealDiv
x"T
y"T
z"T"
Ttype:
2	
A
Relu
features"T
activations"T"
Ttype:
2		
S
ReluGrad
	gradients"T
features"T
	backprops"T"
Ttype:
2		
[
Reshape
tensor"T
shape"Tshape
output"T"	
Ttype"
Tshapetype0:
2	
l
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0
i
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0
M
ScalarSummary
tags
values"T
summary"
Ttype:
2		
P
Shape

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
H
ShardedFilename
basename	
shard

num_shards
filename
O
Size

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
a
Slice

input"T
begin"Index
size"Index
output"T"	
Ttype"
Indextype:
2	
i
SoftmaxCrossEntropyWithLogits
features"T
labels"T	
loss"T
backprop"T"
Ttype:
2
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
5
Sub
x"T
y"T
z"T"
Ttype:
	2	

Sum

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( "
Ttype:
2	"
Tidxtype0:
2	
c
Tile

input"T
	multiples"
Tmultiples
output"T"	
Ttype"

Tmultiplestype0:
2	
s

VariableV2
ref"dtype"
shapeshape"
dtypetype"
	containerstring "
shared_namestring 
&
	ZerosLike
x"T
y"T"	
Ttype"train*1.1.02v1.1.0-rc0-61-g1ec6ed5Îé
\
XPlaceholder*
dtype0*0
_output_shapes
:˙˙˙˙˙˙˙˙˙<*
shape: 
S
YPlaceholder*
dtype0*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
*
shape: 
X
keep_prob/PlaceholderPlaceholder*
dtype0*
_output_shapes
:*
shape: 
]
keep_prob_conv/PlaceholderPlaceholder*
_output_shapes
:*
shape: *
dtype0
N
PlaceholderPlaceholder*
shape: *
dtype0
*
_output_shapes
:

(Conv1/W/Initializer/random_uniform/shapeConst*
dtype0*
_output_shapes
:*
_class
loc:@Conv1/W*%
valueB"            

&Conv1/W/Initializer/random_uniform/minConst*
_class
loc:@Conv1/W*
valueB
 *ŠČ˝*
dtype0*
_output_shapes
: 

&Conv1/W/Initializer/random_uniform/maxConst*
dtype0*
_output_shapes
: *
_class
loc:@Conv1/W*
valueB
 *ŠČ=
Ţ
0Conv1/W/Initializer/random_uniform/RandomUniformRandomUniform(Conv1/W/Initializer/random_uniform/shape*
dtype0*&
_output_shapes
:*

seed*
T0*
_class
loc:@Conv1/W*
seed2
ş
&Conv1/W/Initializer/random_uniform/subSub&Conv1/W/Initializer/random_uniform/max&Conv1/W/Initializer/random_uniform/min*
T0*
_class
loc:@Conv1/W*
_output_shapes
: 
Ô
&Conv1/W/Initializer/random_uniform/mulMul0Conv1/W/Initializer/random_uniform/RandomUniform&Conv1/W/Initializer/random_uniform/sub*&
_output_shapes
:*
T0*
_class
loc:@Conv1/W
Ć
"Conv1/W/Initializer/random_uniformAdd&Conv1/W/Initializer/random_uniform/mul&Conv1/W/Initializer/random_uniform/min*
T0*
_class
loc:@Conv1/W*&
_output_shapes
:
§
Conv1/W
VariableV2*&
_output_shapes
:*
shared_name *
_class
loc:@Conv1/W*
	container *
shape:*
dtype0
ť
Conv1/W/AssignAssignConv1/W"Conv1/W/Initializer/random_uniform*&
_output_shapes
:*
use_locking(*
T0*
_class
loc:@Conv1/W*
validate_shape(
n
Conv1/W/readIdentityConv1/W*
T0*
_class
loc:@Conv1/W*&
_output_shapes
:
l
Conv1/dropout/ShapeConst*%
valueB"            *
dtype0*
_output_shapes
:
e
 Conv1/dropout/random_uniform/minConst*
valueB
 *    *
dtype0*
_output_shapes
: 
e
 Conv1/dropout/random_uniform/maxConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 
§
*Conv1/dropout/random_uniform/RandomUniformRandomUniformConv1/dropout/Shape*
T0*
dtype0*&
_output_shapes
:*
seed2*

seed

 Conv1/dropout/random_uniform/subSub Conv1/dropout/random_uniform/max Conv1/dropout/random_uniform/min*
T0*
_output_shapes
: 
Ś
 Conv1/dropout/random_uniform/mulMul*Conv1/dropout/random_uniform/RandomUniform Conv1/dropout/random_uniform/sub*&
_output_shapes
:*
T0

Conv1/dropout/random_uniformAdd Conv1/dropout/random_uniform/mul Conv1/dropout/random_uniform/min*
T0*&
_output_shapes
:
u
Conv1/dropout/addAddkeep_prob_conv/PlaceholderConv1/dropout/random_uniform*
T0*
_output_shapes
:
R
Conv1/dropout/FloorFloorConv1/dropout/add*
_output_shapes
:*
T0
i
Conv1/dropout/divRealDivConv1/W/readkeep_prob_conv/Placeholder*
_output_shapes
:*
T0
q
Conv1/dropout/mulMulConv1/dropout/divConv1/dropout/Floor*
T0*&
_output_shapes
:

Conv1/b/Initializer/ConstConst*
_class
loc:@Conv1/b*
valueB*    *
dtype0*
_output_shapes
:

Conv1/b
VariableV2*
shape:*
dtype0*
_output_shapes
:*
shared_name *
_class
loc:@Conv1/b*
	container 
Ś
Conv1/b/AssignAssignConv1/bConv1/b/Initializer/Const*
_class
loc:@Conv1/b*
validate_shape(*
_output_shapes
:*
use_locking(*
T0
b
Conv1/b/readIdentityConv1/b*
_output_shapes
:*
T0*
_class
loc:@Conv1/b
ž
Conv1/Conv2DConv2DXConv1/dropout/mul*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
paddingVALID*/
_output_shapes
:˙˙˙˙˙˙˙˙˙8~*
T0

Conv1/BiasAddBiasAddConv1/Conv2DConv1/b/read*
T0*
data_formatNHWC*/
_output_shapes
:˙˙˙˙˙˙˙˙˙8~
[

Conv1/ReluReluConv1/BiasAdd*
T0*/
_output_shapes
:˙˙˙˙˙˙˙˙˙8~
ą
Conv1/MaxPoolMaxPool
Conv1/Relu*
T0*
data_formatNHWC*
strides
*
ksize
*
paddingVALID*/
_output_shapes
:˙˙˙˙˙˙˙˙˙?
_
Conv1/weights/tagConst*
valueB BConv1/weights*
dtype0*
_output_shapes
: 
c
Conv1/weightsHistogramSummaryConv1/weights/tagConv1/W/read*
T0*
_output_shapes
: 
]
Conv1/biases/tagConst*
valueB BConv1/biases*
dtype0*
_output_shapes
: 
a
Conv1/biasesHistogramSummaryConv1/biases/tagConv1/b/read*
T0*
_output_shapes
: 

(Conv2/W/Initializer/random_uniform/shapeConst*
_class
loc:@Conv2/W*%
valueB"         0   *
dtype0*
_output_shapes
:

&Conv2/W/Initializer/random_uniform/minConst*
_class
loc:@Conv2/W*
valueB
 *{l˝*
dtype0*
_output_shapes
: 

&Conv2/W/Initializer/random_uniform/maxConst*
_class
loc:@Conv2/W*
valueB
 *{l=*
dtype0*
_output_shapes
: 
Ţ
0Conv2/W/Initializer/random_uniform/RandomUniformRandomUniform(Conv2/W/Initializer/random_uniform/shape*
dtype0*&
_output_shapes
:0*

seed*
T0*
_class
loc:@Conv2/W*
seed2)
ş
&Conv2/W/Initializer/random_uniform/subSub&Conv2/W/Initializer/random_uniform/max&Conv2/W/Initializer/random_uniform/min*
_class
loc:@Conv2/W*
_output_shapes
: *
T0
Ô
&Conv2/W/Initializer/random_uniform/mulMul0Conv2/W/Initializer/random_uniform/RandomUniform&Conv2/W/Initializer/random_uniform/sub*
T0*
_class
loc:@Conv2/W*&
_output_shapes
:0
Ć
"Conv2/W/Initializer/random_uniformAdd&Conv2/W/Initializer/random_uniform/mul&Conv2/W/Initializer/random_uniform/min*
T0*
_class
loc:@Conv2/W*&
_output_shapes
:0
§
Conv2/W
VariableV2*
shape:0*
dtype0*&
_output_shapes
:0*
shared_name *
_class
loc:@Conv2/W*
	container 
ť
Conv2/W/AssignAssignConv2/W"Conv2/W/Initializer/random_uniform*
use_locking(*
T0*
_class
loc:@Conv2/W*
validate_shape(*&
_output_shapes
:0
n
Conv2/W/readIdentityConv2/W*&
_output_shapes
:0*
T0*
_class
loc:@Conv2/W
l
Conv2/dropout/ShapeConst*%
valueB"         0   *
dtype0*
_output_shapes
:
e
 Conv2/dropout/random_uniform/minConst*
valueB
 *    *
dtype0*
_output_shapes
: 
e
 Conv2/dropout/random_uniform/maxConst*
dtype0*
_output_shapes
: *
valueB
 *  ?
§
*Conv2/dropout/random_uniform/RandomUniformRandomUniformConv2/dropout/Shape*
T0*
dtype0*&
_output_shapes
:0*
seed23*

seed

 Conv2/dropout/random_uniform/subSub Conv2/dropout/random_uniform/max Conv2/dropout/random_uniform/min*
T0*
_output_shapes
: 
Ś
 Conv2/dropout/random_uniform/mulMul*Conv2/dropout/random_uniform/RandomUniform Conv2/dropout/random_uniform/sub*&
_output_shapes
:0*
T0

Conv2/dropout/random_uniformAdd Conv2/dropout/random_uniform/mul Conv2/dropout/random_uniform/min*&
_output_shapes
:0*
T0
u
Conv2/dropout/addAddkeep_prob_conv/PlaceholderConv2/dropout/random_uniform*
T0*
_output_shapes
:
R
Conv2/dropout/FloorFloorConv2/dropout/add*
T0*
_output_shapes
:
i
Conv2/dropout/divRealDivConv2/W/readkeep_prob_conv/Placeholder*
T0*
_output_shapes
:
q
Conv2/dropout/mulMulConv2/dropout/divConv2/dropout/Floor*
T0*&
_output_shapes
:0

Conv2/b/Initializer/ConstConst*
_class
loc:@Conv2/b*
valueB0*    *
dtype0*
_output_shapes
:0

Conv2/b
VariableV2*
	container *
shape:0*
dtype0*
_output_shapes
:0*
shared_name *
_class
loc:@Conv2/b
Ś
Conv2/b/AssignAssignConv2/bConv2/b/Initializer/Const*
use_locking(*
T0*
_class
loc:@Conv2/b*
validate_shape(*
_output_shapes
:0
b
Conv2/b/readIdentityConv2/b*
_output_shapes
:0*
T0*
_class
loc:@Conv2/b
Ę
Conv2/Conv2DConv2DConv1/MaxPoolConv2/dropout/mul*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
paddingVALID*/
_output_shapes
:˙˙˙˙˙˙˙˙˙;0

Conv2/BiasAddBiasAddConv2/Conv2DConv2/b/read*
data_formatNHWC*/
_output_shapes
:˙˙˙˙˙˙˙˙˙;0*
T0
[

Conv2/ReluReluConv2/BiasAdd*/
_output_shapes
:˙˙˙˙˙˙˙˙˙;0*
T0
ą
Conv2/MaxPoolMaxPool
Conv2/Relu*/
_output_shapes
:˙˙˙˙˙˙˙˙˙0*
T0*
data_formatNHWC*
strides
*
ksize
*
paddingVALID
_
Conv2/weights/tagConst*
valueB BConv2/weights*
dtype0*
_output_shapes
: 
c
Conv2/weightsHistogramSummaryConv2/weights/tagConv2/W/read*
T0*
_output_shapes
: 
]
Conv2/biases/tagConst*
valueB BConv2/biases*
dtype0*
_output_shapes
: 
a
Conv2/biasesHistogramSummaryConv2/biases/tagConv2/b/read*
T0*
_output_shapes
: 

(Conv3/W/Initializer/random_uniform/shapeConst*
_class
loc:@Conv3/W*%
valueB"      0   0   *
dtype0*
_output_shapes
:

&Conv3/W/Initializer/random_uniform/minConst*
dtype0*
_output_shapes
: *
_class
loc:@Conv3/W*
valueB
 *ÍĚL˝

&Conv3/W/Initializer/random_uniform/maxConst*
_class
loc:@Conv3/W*
valueB
 *ÍĚL=*
dtype0*
_output_shapes
: 
Ţ
0Conv3/W/Initializer/random_uniform/RandomUniformRandomUniform(Conv3/W/Initializer/random_uniform/shape*
T0*
_class
loc:@Conv3/W*
seed2J*
dtype0*&
_output_shapes
:00*

seed
ş
&Conv3/W/Initializer/random_uniform/subSub&Conv3/W/Initializer/random_uniform/max&Conv3/W/Initializer/random_uniform/min*
T0*
_class
loc:@Conv3/W*
_output_shapes
: 
Ô
&Conv3/W/Initializer/random_uniform/mulMul0Conv3/W/Initializer/random_uniform/RandomUniform&Conv3/W/Initializer/random_uniform/sub*
T0*
_class
loc:@Conv3/W*&
_output_shapes
:00
Ć
"Conv3/W/Initializer/random_uniformAdd&Conv3/W/Initializer/random_uniform/mul&Conv3/W/Initializer/random_uniform/min*
_class
loc:@Conv3/W*&
_output_shapes
:00*
T0
§
Conv3/W
VariableV2*
	container *
shape:00*
dtype0*&
_output_shapes
:00*
shared_name *
_class
loc:@Conv3/W
ť
Conv3/W/AssignAssignConv3/W"Conv3/W/Initializer/random_uniform*
validate_shape(*&
_output_shapes
:00*
use_locking(*
T0*
_class
loc:@Conv3/W
n
Conv3/W/readIdentityConv3/W*
T0*
_class
loc:@Conv3/W*&
_output_shapes
:00
l
Conv3/dropout/ShapeConst*%
valueB"      0   0   *
dtype0*
_output_shapes
:
e
 Conv3/dropout/random_uniform/minConst*
valueB
 *    *
dtype0*
_output_shapes
: 
e
 Conv3/dropout/random_uniform/maxConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 
§
*Conv3/dropout/random_uniform/RandomUniformRandomUniformConv3/dropout/Shape*

seed*
T0*
dtype0*&
_output_shapes
:00*
seed2T

 Conv3/dropout/random_uniform/subSub Conv3/dropout/random_uniform/max Conv3/dropout/random_uniform/min*
T0*
_output_shapes
: 
Ś
 Conv3/dropout/random_uniform/mulMul*Conv3/dropout/random_uniform/RandomUniform Conv3/dropout/random_uniform/sub*&
_output_shapes
:00*
T0

Conv3/dropout/random_uniformAdd Conv3/dropout/random_uniform/mul Conv3/dropout/random_uniform/min*&
_output_shapes
:00*
T0
u
Conv3/dropout/addAddkeep_prob_conv/PlaceholderConv3/dropout/random_uniform*
T0*
_output_shapes
:
R
Conv3/dropout/FloorFloorConv3/dropout/add*
_output_shapes
:*
T0
i
Conv3/dropout/divRealDivConv3/W/readkeep_prob_conv/Placeholder*
T0*
_output_shapes
:
q
Conv3/dropout/mulMulConv3/dropout/divConv3/dropout/Floor*
T0*&
_output_shapes
:00

Conv3/b/Initializer/ConstConst*
_class
loc:@Conv3/b*
valueB0*    *
dtype0*
_output_shapes
:0

Conv3/b
VariableV2*
_output_shapes
:0*
shared_name *
_class
loc:@Conv3/b*
	container *
shape:0*
dtype0
Ś
Conv3/b/AssignAssignConv3/bConv3/b/Initializer/Const*
use_locking(*
T0*
_class
loc:@Conv3/b*
validate_shape(*
_output_shapes
:0
b
Conv3/b/readIdentityConv3/b*
T0*
_class
loc:@Conv3/b*
_output_shapes
:0
Ę
Conv3/Conv2DConv2DConv2/MaxPoolConv3/dropout/mul*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
paddingVALID*/
_output_shapes
:˙˙˙˙˙˙˙˙˙0

Conv3/BiasAddBiasAddConv3/Conv2DConv3/b/read*
data_formatNHWC*/
_output_shapes
:˙˙˙˙˙˙˙˙˙0*
T0
[

Conv3/ReluReluConv3/BiasAdd*
T0*/
_output_shapes
:˙˙˙˙˙˙˙˙˙0
ą
Conv3/MaxPoolMaxPool
Conv3/Relu*
data_formatNHWC*
strides
*
ksize
*
paddingVALID*/
_output_shapes
:˙˙˙˙˙˙˙˙˙0*
T0
_
Conv3/weights/tagConst*
_output_shapes
: *
valueB BConv3/weights*
dtype0
c
Conv3/weightsHistogramSummaryConv3/weights/tagConv3/W/read*
T0*
_output_shapes
: 
]
Conv3/biases/tagConst*
_output_shapes
: *
valueB BConv3/biases*
dtype0
a
Conv3/biasesHistogramSummaryConv3/biases/tagConv3/b/read*
T0*
_output_shapes
: 
f
Flatten/Reshape/shapeConst*
valueB"˙˙˙˙ 	  *
dtype0*
_output_shapes
:

Flatten/ReshapeReshapeConv3/MaxPoolFlatten/Reshape/shape*
T0*
Tshape0*(
_output_shapes
:˙˙˙˙˙˙˙˙˙

&FC1/W/Initializer/random_uniform/shapeConst*
_class

loc:@FC1/W*
valueB" 	  @   *
dtype0*
_output_shapes
:

$FC1/W/Initializer/random_uniform/minConst*
_class

loc:@FC1/W*
valueB
 *Ü-N˝*
dtype0*
_output_shapes
: 

$FC1/W/Initializer/random_uniform/maxConst*
_output_shapes
: *
_class

loc:@FC1/W*
valueB
 *Ü-N=*
dtype0
Ń
.FC1/W/Initializer/random_uniform/RandomUniformRandomUniform&FC1/W/Initializer/random_uniform/shape*
seed2m*
dtype0*
_output_shapes
:	@*

seed*
T0*
_class

loc:@FC1/W
˛
$FC1/W/Initializer/random_uniform/subSub$FC1/W/Initializer/random_uniform/max$FC1/W/Initializer/random_uniform/min*
T0*
_class

loc:@FC1/W*
_output_shapes
: 
Ĺ
$FC1/W/Initializer/random_uniform/mulMul.FC1/W/Initializer/random_uniform/RandomUniform$FC1/W/Initializer/random_uniform/sub*
T0*
_class

loc:@FC1/W*
_output_shapes
:	@
ˇ
 FC1/W/Initializer/random_uniformAdd$FC1/W/Initializer/random_uniform/mul$FC1/W/Initializer/random_uniform/min*
_class

loc:@FC1/W*
_output_shapes
:	@*
T0

FC1/W
VariableV2*
shared_name *
_class

loc:@FC1/W*
	container *
shape:	@*
dtype0*
_output_shapes
:	@
Ź
FC1/W/AssignAssignFC1/W FC1/W/Initializer/random_uniform*
_output_shapes
:	@*
use_locking(*
T0*
_class

loc:@FC1/W*
validate_shape(
a

FC1/W/readIdentityFC1/W*
T0*
_class

loc:@FC1/W*
_output_shapes
:	@
~
FC1/b/Initializer/ConstConst*
_class

loc:@FC1/b*
valueB@*    *
dtype0*
_output_shapes
:@

FC1/b
VariableV2*
dtype0*
_output_shapes
:@*
shared_name *
_class

loc:@FC1/b*
	container *
shape:@

FC1/b/AssignAssignFC1/bFC1/b/Initializer/Const*
validate_shape(*
_output_shapes
:@*
use_locking(*
T0*
_class

loc:@FC1/b
\

FC1/b/readIdentityFC1/b*
T0*
_class

loc:@FC1/b*
_output_shapes
:@
`
FC1/dropout/ShapeShapeFlatten/Reshape*
T0*
out_type0*
_output_shapes
:
c
FC1/dropout/random_uniform/minConst*
valueB
 *    *
dtype0*
_output_shapes
: 
c
FC1/dropout/random_uniform/maxConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 
Ľ
(FC1/dropout/random_uniform/RandomUniformRandomUniformFC1/dropout/Shape*
dtype0*(
_output_shapes
:˙˙˙˙˙˙˙˙˙*
seed2{*

seed*
T0

FC1/dropout/random_uniform/subSubFC1/dropout/random_uniform/maxFC1/dropout/random_uniform/min*
T0*
_output_shapes
: 
˘
FC1/dropout/random_uniform/mulMul(FC1/dropout/random_uniform/RandomUniformFC1/dropout/random_uniform/sub*(
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0

FC1/dropout/random_uniformAddFC1/dropout/random_uniform/mulFC1/dropout/random_uniform/min*
T0*(
_output_shapes
:˙˙˙˙˙˙˙˙˙
l
FC1/dropout/addAddkeep_prob/PlaceholderFC1/dropout/random_uniform*
T0*
_output_shapes
:
N
FC1/dropout/FloorFloorFC1/dropout/add*
_output_shapes
:*
T0
e
FC1/dropout/divRealDivFlatten/Reshapekeep_prob/Placeholder*
T0*
_output_shapes
:
m
FC1/dropout/mulMulFC1/dropout/divFC1/dropout/Floor*(
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0


FC1/MatMulMatMulFC1/dropout/mul
FC1/W/read*'
_output_shapes
:˙˙˙˙˙˙˙˙˙@*
transpose_a( *
transpose_b( *
T0
X
FC1/addAdd
FC1/MatMul
FC1/b/read*
T0*'
_output_shapes
:˙˙˙˙˙˙˙˙˙@
K
FC1/ReluReluFC1/add*'
_output_shapes
:˙˙˙˙˙˙˙˙˙@*
T0

&FC2/W/Initializer/random_uniform/shapeConst*
dtype0*
_output_shapes
:*
_class

loc:@FC2/W*
valueB"@   
   

$FC2/W/Initializer/random_uniform/minConst*
_class

loc:@FC2/W*
valueB
 *iĘž*
dtype0*
_output_shapes
: 

$FC2/W/Initializer/random_uniform/maxConst*
_class

loc:@FC2/W*
valueB
 *iĘ>*
dtype0*
_output_shapes
: 
Ń
.FC2/W/Initializer/random_uniform/RandomUniformRandomUniform&FC2/W/Initializer/random_uniform/shape*
dtype0*
_output_shapes

:@
*

seed*
T0*
_class

loc:@FC2/W*
seed2
˛
$FC2/W/Initializer/random_uniform/subSub$FC2/W/Initializer/random_uniform/max$FC2/W/Initializer/random_uniform/min*
T0*
_class

loc:@FC2/W*
_output_shapes
: 
Ä
$FC2/W/Initializer/random_uniform/mulMul.FC2/W/Initializer/random_uniform/RandomUniform$FC2/W/Initializer/random_uniform/sub*
T0*
_class

loc:@FC2/W*
_output_shapes

:@

ś
 FC2/W/Initializer/random_uniformAdd$FC2/W/Initializer/random_uniform/mul$FC2/W/Initializer/random_uniform/min*
_class

loc:@FC2/W*
_output_shapes

:@
*
T0

FC2/W
VariableV2*
dtype0*
_output_shapes

:@
*
shared_name *
_class

loc:@FC2/W*
	container *
shape
:@

Ť
FC2/W/AssignAssignFC2/W FC2/W/Initializer/random_uniform*
use_locking(*
T0*
_class

loc:@FC2/W*
validate_shape(*
_output_shapes

:@

`

FC2/W/readIdentityFC2/W*
T0*
_class

loc:@FC2/W*
_output_shapes

:@

~
FC2/b/Initializer/ConstConst*
_class

loc:@FC2/b*
valueB
*    *
dtype0*
_output_shapes
:


FC2/b
VariableV2*
_class

loc:@FC2/b*
	container *
shape:
*
dtype0*
_output_shapes
:
*
shared_name 

FC2/b/AssignAssignFC2/bFC2/b/Initializer/Const*
validate_shape(*
_output_shapes
:
*
use_locking(*
T0*
_class

loc:@FC2/b
\

FC2/b/readIdentityFC2/b*
T0*
_class

loc:@FC2/b*
_output_shapes
:

Y
FC2/dropout/ShapeShapeFC1/Relu*
_output_shapes
:*
T0*
out_type0
c
FC2/dropout/random_uniform/minConst*
_output_shapes
: *
valueB
 *    *
dtype0
c
FC2/dropout/random_uniform/maxConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 
Ľ
(FC2/dropout/random_uniform/RandomUniformRandomUniformFC2/dropout/Shape*
T0*
dtype0*'
_output_shapes
:˙˙˙˙˙˙˙˙˙@*
seed2*

seed

FC2/dropout/random_uniform/subSubFC2/dropout/random_uniform/maxFC2/dropout/random_uniform/min*
_output_shapes
: *
T0
Ą
FC2/dropout/random_uniform/mulMul(FC2/dropout/random_uniform/RandomUniformFC2/dropout/random_uniform/sub*'
_output_shapes
:˙˙˙˙˙˙˙˙˙@*
T0

FC2/dropout/random_uniformAddFC2/dropout/random_uniform/mulFC2/dropout/random_uniform/min*
T0*'
_output_shapes
:˙˙˙˙˙˙˙˙˙@
l
FC2/dropout/addAddkeep_prob/PlaceholderFC2/dropout/random_uniform*
T0*
_output_shapes
:
N
FC2/dropout/FloorFloorFC2/dropout/add*
T0*
_output_shapes
:
^
FC2/dropout/divRealDivFC1/Relukeep_prob/Placeholder*
_output_shapes
:*
T0
l
FC2/dropout/mulMulFC2/dropout/divFC2/dropout/Floor*'
_output_shapes
:˙˙˙˙˙˙˙˙˙@*
T0


FC2/MatMulMatMulFC2/dropout/mul
FC2/W/read*
T0*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
*
transpose_a( *
transpose_b( 
X
FC2/addAdd
FC2/MatMul
FC2/b/read*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
*
T0
Y
Cost/cross_entropy/RankConst*
value	B :*
dtype0*
_output_shapes
: 
_
Cost/cross_entropy/ShapeShapeFC2/add*
T0*
out_type0*
_output_shapes
:
[
Cost/cross_entropy/Rank_1Const*
value	B :*
dtype0*
_output_shapes
: 
a
Cost/cross_entropy/Shape_1ShapeFC2/add*
T0*
out_type0*
_output_shapes
:
Z
Cost/cross_entropy/Sub/yConst*
value	B :*
dtype0*
_output_shapes
: 
s
Cost/cross_entropy/SubSubCost/cross_entropy/Rank_1Cost/cross_entropy/Sub/y*
_output_shapes
: *
T0
x
Cost/cross_entropy/Slice/beginPackCost/cross_entropy/Sub*
T0*

axis *
N*
_output_shapes
:
g
Cost/cross_entropy/Slice/sizeConst*
valueB:*
dtype0*
_output_shapes
:
Ž
Cost/cross_entropy/SliceSliceCost/cross_entropy/Shape_1Cost/cross_entropy/Slice/beginCost/cross_entropy/Slice/size*
_output_shapes
:*
Index0*
T0
u
"Cost/cross_entropy/concat/values_0Const*
_output_shapes
:*
valueB:
˙˙˙˙˙˙˙˙˙*
dtype0
`
Cost/cross_entropy/concat/axisConst*
_output_shapes
: *
value	B : *
dtype0
˝
Cost/cross_entropy/concatConcatV2"Cost/cross_entropy/concat/values_0Cost/cross_entropy/SliceCost/cross_entropy/concat/axis*
T0*
N*
_output_shapes
:*

Tidx0

Cost/cross_entropy/ReshapeReshapeFC2/addCost/cross_entropy/concat*
T0*
Tshape0*0
_output_shapes
:˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
[
Cost/cross_entropy/Rank_2Const*
value	B :*
dtype0*
_output_shapes
: 
[
Cost/cross_entropy/Shape_2ShapeY*
_output_shapes
:*
T0*
out_type0
\
Cost/cross_entropy/Sub_1/yConst*
value	B :*
dtype0*
_output_shapes
: 
w
Cost/cross_entropy/Sub_1SubCost/cross_entropy/Rank_2Cost/cross_entropy/Sub_1/y*
T0*
_output_shapes
: 
|
 Cost/cross_entropy/Slice_1/beginPackCost/cross_entropy/Sub_1*
N*
_output_shapes
:*
T0*

axis 
i
Cost/cross_entropy/Slice_1/sizeConst*
valueB:*
dtype0*
_output_shapes
:
´
Cost/cross_entropy/Slice_1SliceCost/cross_entropy/Shape_2 Cost/cross_entropy/Slice_1/beginCost/cross_entropy/Slice_1/size*
Index0*
T0*
_output_shapes
:
w
$Cost/cross_entropy/concat_1/values_0Const*
valueB:
˙˙˙˙˙˙˙˙˙*
dtype0*
_output_shapes
:
b
 Cost/cross_entropy/concat_1/axisConst*
value	B : *
dtype0*
_output_shapes
: 
Ĺ
Cost/cross_entropy/concat_1ConcatV2$Cost/cross_entropy/concat_1/values_0Cost/cross_entropy/Slice_1 Cost/cross_entropy/concat_1/axis*
N*
_output_shapes
:*

Tidx0*
T0

Cost/cross_entropy/Reshape_1ReshapeYCost/cross_entropy/concat_1*
T0*
Tshape0*0
_output_shapes
:˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
Ő
0Cost/cross_entropy/SoftmaxCrossEntropyWithLogitsSoftmaxCrossEntropyWithLogitsCost/cross_entropy/ReshapeCost/cross_entropy/Reshape_1*
T0*?
_output_shapes-
+:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
\
Cost/cross_entropy/Sub_2/yConst*
value	B :*
dtype0*
_output_shapes
: 
u
Cost/cross_entropy/Sub_2SubCost/cross_entropy/RankCost/cross_entropy/Sub_2/y*
_output_shapes
: *
T0
j
 Cost/cross_entropy/Slice_2/beginConst*
valueB: *
dtype0*
_output_shapes
:
{
Cost/cross_entropy/Slice_2/sizePackCost/cross_entropy/Sub_2*
T0*

axis *
N*
_output_shapes
:
ť
Cost/cross_entropy/Slice_2SliceCost/cross_entropy/Shape Cost/cross_entropy/Slice_2/beginCost/cross_entropy/Slice_2/size*
Index0*
T0*#
_output_shapes
:˙˙˙˙˙˙˙˙˙
ą
Cost/cross_entropy/Reshape_2Reshape0Cost/cross_entropy/SoftmaxCrossEntropyWithLogitsCost/cross_entropy/Slice_2*
T0*
Tshape0*#
_output_shapes
:˙˙˙˙˙˙˙˙˙
b
Cost/cross_entropy/ConstConst*
_output_shapes
:*
valueB: *
dtype0

Cost/cross_entropy/MeanMeanCost/cross_entropy/Reshape_2Cost/cross_entropy/Const*
_output_shapes
: *

Tidx0*
	keep_dims( *
T0
W
Cost/L2_regularization/SizeSizeY*
T0*
out_type0*
_output_shapes
: 
b
 Cost/L2_regularization/truediv/yConst*
value	B :
*
dtype0*
_output_shapes
: 
x
#Cost/L2_regularization/truediv/CastCastCost/L2_regularization/Size*

SrcT0*
_output_shapes
: *

DstT0

%Cost/L2_regularization/truediv/Cast_1Cast Cost/L2_regularization/truediv/y*

SrcT0*
_output_shapes
: *

DstT0

Cost/L2_regularization/truedivRealDiv#Cost/L2_regularization/truediv/Cast%Cost/L2_regularization/truediv/Cast_1*
T0*
_output_shapes
: 
s
Cost/L2_regularization/CastCastCost/L2_regularization/truediv*
_output_shapes
: *

DstT0*

SrcT0
V
Cost/L2_regularization/L2LossL2LossConv1/W/read*
_output_shapes
: *
T0
X
Cost/L2_regularization/L2Loss_1L2LossConv2/W/read*
T0*
_output_shapes
: 

Cost/L2_regularization/addAddCost/L2_regularization/L2LossCost/L2_regularization/L2Loss_1*
_output_shapes
: *
T0
X
Cost/L2_regularization/L2Loss_2L2LossConv3/W/read*
T0*
_output_shapes
: 

Cost/L2_regularization/add_1AddCost/L2_regularization/addCost/L2_regularization/L2Loss_2*
T0*
_output_shapes
: 
V
Cost/L2_regularization/L2Loss_3L2Loss
FC1/W/read*
_output_shapes
: *
T0

Cost/L2_regularization/add_2AddCost/L2_regularization/add_1Cost/L2_regularization/L2Loss_3*
_output_shapes
: *
T0
V
Cost/L2_regularization/L2Loss_4L2Loss
FC2/W/read*
T0*
_output_shapes
: 

Cost/L2_regularization/add_3AddCost/L2_regularization/add_2Cost/L2_regularization/L2Loss_4*
T0*
_output_shapes
: 
Z
Cost/total_cost/Mul/xConst*
_output_shapes
: *
valueB
 *
×Ł;*
dtype0
p
Cost/total_cost/MulMulCost/total_cost/Mul/xCost/L2_regularization/add_3*
_output_shapes
: *
T0
i
Cost/total_cost/addAddCost/cross_entropy/MeanCost/total_cost/Mul*
_output_shapes
: *
T0
~
!Cost/per_epoch_per_minibatch/tagsConst*-
value$B" BCost/per_epoch_per_minibatch*
dtype0*
_output_shapes
: 

Cost/per_epoch_per_minibatchScalarSummary!Cost/per_epoch_per_minibatch/tagsCost/total_cost/add*
_output_shapes
: *
T0
R
gradients/ShapeConst*
valueB *
dtype0*
_output_shapes
: 
T
gradients/ConstConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 
Y
gradients/FillFillgradients/Shapegradients/Const*
T0*
_output_shapes
: 
k
(gradients/Cost/total_cost/add_grad/ShapeConst*
valueB *
dtype0*
_output_shapes
: 
m
*gradients/Cost/total_cost/add_grad/Shape_1Const*
valueB *
dtype0*
_output_shapes
: 
ä
8gradients/Cost/total_cost/add_grad/BroadcastGradientArgsBroadcastGradientArgs(gradients/Cost/total_cost/add_grad/Shape*gradients/Cost/total_cost/add_grad/Shape_1*
T0*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙
ˇ
&gradients/Cost/total_cost/add_grad/SumSumgradients/Fill8gradients/Cost/total_cost/add_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
ś
*gradients/Cost/total_cost/add_grad/ReshapeReshape&gradients/Cost/total_cost/add_grad/Sum(gradients/Cost/total_cost/add_grad/Shape*
T0*
Tshape0*
_output_shapes
: 
ť
(gradients/Cost/total_cost/add_grad/Sum_1Sumgradients/Fill:gradients/Cost/total_cost/add_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
ź
,gradients/Cost/total_cost/add_grad/Reshape_1Reshape(gradients/Cost/total_cost/add_grad/Sum_1*gradients/Cost/total_cost/add_grad/Shape_1*
T0*
Tshape0*
_output_shapes
: 

3gradients/Cost/total_cost/add_grad/tuple/group_depsNoOp+^gradients/Cost/total_cost/add_grad/Reshape-^gradients/Cost/total_cost/add_grad/Reshape_1

;gradients/Cost/total_cost/add_grad/tuple/control_dependencyIdentity*gradients/Cost/total_cost/add_grad/Reshape4^gradients/Cost/total_cost/add_grad/tuple/group_deps*
T0*=
_class3
1/loc:@gradients/Cost/total_cost/add_grad/Reshape*
_output_shapes
: 

=gradients/Cost/total_cost/add_grad/tuple/control_dependency_1Identity,gradients/Cost/total_cost/add_grad/Reshape_14^gradients/Cost/total_cost/add_grad/tuple/group_deps*
_output_shapes
: *
T0*?
_class5
31loc:@gradients/Cost/total_cost/add_grad/Reshape_1
~
4gradients/Cost/cross_entropy/Mean_grad/Reshape/shapeConst*
dtype0*
_output_shapes
:*
valueB:
ß
.gradients/Cost/cross_entropy/Mean_grad/ReshapeReshape;gradients/Cost/total_cost/add_grad/tuple/control_dependency4gradients/Cost/cross_entropy/Mean_grad/Reshape/shape*
T0*
Tshape0*
_output_shapes
:

,gradients/Cost/cross_entropy/Mean_grad/ShapeShapeCost/cross_entropy/Reshape_2*
T0*
out_type0*
_output_shapes
:
Ń
+gradients/Cost/cross_entropy/Mean_grad/TileTile.gradients/Cost/cross_entropy/Mean_grad/Reshape,gradients/Cost/cross_entropy/Mean_grad/Shape*

Tmultiples0*
T0*#
_output_shapes
:˙˙˙˙˙˙˙˙˙

.gradients/Cost/cross_entropy/Mean_grad/Shape_1ShapeCost/cross_entropy/Reshape_2*
T0*
out_type0*
_output_shapes
:
q
.gradients/Cost/cross_entropy/Mean_grad/Shape_2Const*
valueB *
dtype0*
_output_shapes
: 
v
,gradients/Cost/cross_entropy/Mean_grad/ConstConst*
valueB: *
dtype0*
_output_shapes
:
Ď
+gradients/Cost/cross_entropy/Mean_grad/ProdProd.gradients/Cost/cross_entropy/Mean_grad/Shape_1,gradients/Cost/cross_entropy/Mean_grad/Const*
_output_shapes
: *

Tidx0*
	keep_dims( *
T0
x
.gradients/Cost/cross_entropy/Mean_grad/Const_1Const*
valueB: *
dtype0*
_output_shapes
:
Ó
-gradients/Cost/cross_entropy/Mean_grad/Prod_1Prod.gradients/Cost/cross_entropy/Mean_grad/Shape_2.gradients/Cost/cross_entropy/Mean_grad/Const_1*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 
r
0gradients/Cost/cross_entropy/Mean_grad/Maximum/yConst*
value	B :*
dtype0*
_output_shapes
: 
ť
.gradients/Cost/cross_entropy/Mean_grad/MaximumMaximum-gradients/Cost/cross_entropy/Mean_grad/Prod_10gradients/Cost/cross_entropy/Mean_grad/Maximum/y*
T0*
_output_shapes
: 
š
/gradients/Cost/cross_entropy/Mean_grad/floordivFloorDiv+gradients/Cost/cross_entropy/Mean_grad/Prod.gradients/Cost/cross_entropy/Mean_grad/Maximum*
T0*
_output_shapes
: 

+gradients/Cost/cross_entropy/Mean_grad/CastCast/gradients/Cost/cross_entropy/Mean_grad/floordiv*

SrcT0*
_output_shapes
: *

DstT0
Á
.gradients/Cost/cross_entropy/Mean_grad/truedivRealDiv+gradients/Cost/cross_entropy/Mean_grad/Tile+gradients/Cost/cross_entropy/Mean_grad/Cast*
T0*#
_output_shapes
:˙˙˙˙˙˙˙˙˙
k
(gradients/Cost/total_cost/Mul_grad/ShapeConst*
valueB *
dtype0*
_output_shapes
: 
m
*gradients/Cost/total_cost/Mul_grad/Shape_1Const*
valueB *
dtype0*
_output_shapes
: 
ä
8gradients/Cost/total_cost/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs(gradients/Cost/total_cost/Mul_grad/Shape*gradients/Cost/total_cost/Mul_grad/Shape_1*
T0*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙
Ť
&gradients/Cost/total_cost/Mul_grad/mulMul=gradients/Cost/total_cost/add_grad/tuple/control_dependency_1Cost/L2_regularization/add_3*
T0*
_output_shapes
: 
Ď
&gradients/Cost/total_cost/Mul_grad/SumSum&gradients/Cost/total_cost/Mul_grad/mul8gradients/Cost/total_cost/Mul_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
ś
*gradients/Cost/total_cost/Mul_grad/ReshapeReshape&gradients/Cost/total_cost/Mul_grad/Sum(gradients/Cost/total_cost/Mul_grad/Shape*
T0*
Tshape0*
_output_shapes
: 
Ś
(gradients/Cost/total_cost/Mul_grad/mul_1MulCost/total_cost/Mul/x=gradients/Cost/total_cost/add_grad/tuple/control_dependency_1*
T0*
_output_shapes
: 
Ő
(gradients/Cost/total_cost/Mul_grad/Sum_1Sum(gradients/Cost/total_cost/Mul_grad/mul_1:gradients/Cost/total_cost/Mul_grad/BroadcastGradientArgs:1*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
ź
,gradients/Cost/total_cost/Mul_grad/Reshape_1Reshape(gradients/Cost/total_cost/Mul_grad/Sum_1*gradients/Cost/total_cost/Mul_grad/Shape_1*
T0*
Tshape0*
_output_shapes
: 

3gradients/Cost/total_cost/Mul_grad/tuple/group_depsNoOp+^gradients/Cost/total_cost/Mul_grad/Reshape-^gradients/Cost/total_cost/Mul_grad/Reshape_1

;gradients/Cost/total_cost/Mul_grad/tuple/control_dependencyIdentity*gradients/Cost/total_cost/Mul_grad/Reshape4^gradients/Cost/total_cost/Mul_grad/tuple/group_deps*
T0*=
_class3
1/loc:@gradients/Cost/total_cost/Mul_grad/Reshape*
_output_shapes
: 

=gradients/Cost/total_cost/Mul_grad/tuple/control_dependency_1Identity,gradients/Cost/total_cost/Mul_grad/Reshape_14^gradients/Cost/total_cost/Mul_grad/tuple/group_deps*?
_class5
31loc:@gradients/Cost/total_cost/Mul_grad/Reshape_1*
_output_shapes
: *
T0
Ą
1gradients/Cost/cross_entropy/Reshape_2_grad/ShapeShape0Cost/cross_entropy/SoftmaxCrossEntropyWithLogits*
_output_shapes
:*
T0*
out_type0
Ý
3gradients/Cost/cross_entropy/Reshape_2_grad/ReshapeReshape.gradients/Cost/cross_entropy/Mean_grad/truediv1gradients/Cost/cross_entropy/Reshape_2_grad/Shape*
T0*
Tshape0*#
_output_shapes
:˙˙˙˙˙˙˙˙˙
t
1gradients/Cost/L2_regularization/add_3_grad/ShapeConst*
_output_shapes
: *
valueB *
dtype0
v
3gradients/Cost/L2_regularization/add_3_grad/Shape_1Const*
valueB *
dtype0*
_output_shapes
: 
˙
Agradients/Cost/L2_regularization/add_3_grad/BroadcastGradientArgsBroadcastGradientArgs1gradients/Cost/L2_regularization/add_3_grad/Shape3gradients/Cost/L2_regularization/add_3_grad/Shape_1*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙*
T0
ř
/gradients/Cost/L2_regularization/add_3_grad/SumSum=gradients/Cost/total_cost/Mul_grad/tuple/control_dependency_1Agradients/Cost/L2_regularization/add_3_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Ń
3gradients/Cost/L2_regularization/add_3_grad/ReshapeReshape/gradients/Cost/L2_regularization/add_3_grad/Sum1gradients/Cost/L2_regularization/add_3_grad/Shape*
T0*
Tshape0*
_output_shapes
: 
ü
1gradients/Cost/L2_regularization/add_3_grad/Sum_1Sum=gradients/Cost/total_cost/Mul_grad/tuple/control_dependency_1Cgradients/Cost/L2_regularization/add_3_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
×
5gradients/Cost/L2_regularization/add_3_grad/Reshape_1Reshape1gradients/Cost/L2_regularization/add_3_grad/Sum_13gradients/Cost/L2_regularization/add_3_grad/Shape_1*
Tshape0*
_output_shapes
: *
T0
˛
<gradients/Cost/L2_regularization/add_3_grad/tuple/group_depsNoOp4^gradients/Cost/L2_regularization/add_3_grad/Reshape6^gradients/Cost/L2_regularization/add_3_grad/Reshape_1
­
Dgradients/Cost/L2_regularization/add_3_grad/tuple/control_dependencyIdentity3gradients/Cost/L2_regularization/add_3_grad/Reshape=^gradients/Cost/L2_regularization/add_3_grad/tuple/group_deps*
T0*F
_class<
:8loc:@gradients/Cost/L2_regularization/add_3_grad/Reshape*
_output_shapes
: 
ł
Fgradients/Cost/L2_regularization/add_3_grad/tuple/control_dependency_1Identity5gradients/Cost/L2_regularization/add_3_grad/Reshape_1=^gradients/Cost/L2_regularization/add_3_grad/tuple/group_deps*
T0*H
_class>
<:loc:@gradients/Cost/L2_regularization/add_3_grad/Reshape_1*
_output_shapes
: 

gradients/zeros_like	ZerosLike2Cost/cross_entropy/SoftmaxCrossEntropyWithLogits:1*
T0*0
_output_shapes
:˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙

Ngradients/Cost/cross_entropy/SoftmaxCrossEntropyWithLogits_grad/ExpandDims/dimConst*
_output_shapes
: *
valueB :
˙˙˙˙˙˙˙˙˙*
dtype0

Jgradients/Cost/cross_entropy/SoftmaxCrossEntropyWithLogits_grad/ExpandDims
ExpandDims3gradients/Cost/cross_entropy/Reshape_2_grad/ReshapeNgradients/Cost/cross_entropy/SoftmaxCrossEntropyWithLogits_grad/ExpandDims/dim*

Tdim0*
T0*'
_output_shapes
:˙˙˙˙˙˙˙˙˙

Cgradients/Cost/cross_entropy/SoftmaxCrossEntropyWithLogits_grad/mulMulJgradients/Cost/cross_entropy/SoftmaxCrossEntropyWithLogits_grad/ExpandDims2Cost/cross_entropy/SoftmaxCrossEntropyWithLogits:1*
T0*0
_output_shapes
:˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
t
1gradients/Cost/L2_regularization/add_2_grad/ShapeConst*
valueB *
dtype0*
_output_shapes
: 
v
3gradients/Cost/L2_regularization/add_2_grad/Shape_1Const*
dtype0*
_output_shapes
: *
valueB 
˙
Agradients/Cost/L2_regularization/add_2_grad/BroadcastGradientArgsBroadcastGradientArgs1gradients/Cost/L2_regularization/add_2_grad/Shape3gradients/Cost/L2_regularization/add_2_grad/Shape_1*
T0*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙
˙
/gradients/Cost/L2_regularization/add_2_grad/SumSumDgradients/Cost/L2_regularization/add_3_grad/tuple/control_dependencyAgradients/Cost/L2_regularization/add_2_grad/BroadcastGradientArgs*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
Ń
3gradients/Cost/L2_regularization/add_2_grad/ReshapeReshape/gradients/Cost/L2_regularization/add_2_grad/Sum1gradients/Cost/L2_regularization/add_2_grad/Shape*
T0*
Tshape0*
_output_shapes
: 

1gradients/Cost/L2_regularization/add_2_grad/Sum_1SumDgradients/Cost/L2_regularization/add_3_grad/tuple/control_dependencyCgradients/Cost/L2_regularization/add_2_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
×
5gradients/Cost/L2_regularization/add_2_grad/Reshape_1Reshape1gradients/Cost/L2_regularization/add_2_grad/Sum_13gradients/Cost/L2_regularization/add_2_grad/Shape_1*
Tshape0*
_output_shapes
: *
T0
˛
<gradients/Cost/L2_regularization/add_2_grad/tuple/group_depsNoOp4^gradients/Cost/L2_regularization/add_2_grad/Reshape6^gradients/Cost/L2_regularization/add_2_grad/Reshape_1
­
Dgradients/Cost/L2_regularization/add_2_grad/tuple/control_dependencyIdentity3gradients/Cost/L2_regularization/add_2_grad/Reshape=^gradients/Cost/L2_regularization/add_2_grad/tuple/group_deps*
T0*F
_class<
:8loc:@gradients/Cost/L2_regularization/add_2_grad/Reshape*
_output_shapes
: 
ł
Fgradients/Cost/L2_regularization/add_2_grad/tuple/control_dependency_1Identity5gradients/Cost/L2_regularization/add_2_grad/Reshape_1=^gradients/Cost/L2_regularization/add_2_grad/tuple/group_deps*
T0*H
_class>
<:loc:@gradients/Cost/L2_regularization/add_2_grad/Reshape_1*
_output_shapes
: 
ś
2gradients/Cost/L2_regularization/L2Loss_4_grad/mulMul
FC2/W/readFgradients/Cost/L2_regularization/add_3_grad/tuple/control_dependency_1*
_output_shapes

:@
*
T0
v
/gradients/Cost/cross_entropy/Reshape_grad/ShapeShapeFC2/add*
_output_shapes
:*
T0*
out_type0
ň
1gradients/Cost/cross_entropy/Reshape_grad/ReshapeReshapeCgradients/Cost/cross_entropy/SoftmaxCrossEntropyWithLogits_grad/mul/gradients/Cost/cross_entropy/Reshape_grad/Shape*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
*
T0*
Tshape0
t
1gradients/Cost/L2_regularization/add_1_grad/ShapeConst*
dtype0*
_output_shapes
: *
valueB 
v
3gradients/Cost/L2_regularization/add_1_grad/Shape_1Const*
valueB *
dtype0*
_output_shapes
: 
˙
Agradients/Cost/L2_regularization/add_1_grad/BroadcastGradientArgsBroadcastGradientArgs1gradients/Cost/L2_regularization/add_1_grad/Shape3gradients/Cost/L2_regularization/add_1_grad/Shape_1*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙*
T0
˙
/gradients/Cost/L2_regularization/add_1_grad/SumSumDgradients/Cost/L2_regularization/add_2_grad/tuple/control_dependencyAgradients/Cost/L2_regularization/add_1_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Ń
3gradients/Cost/L2_regularization/add_1_grad/ReshapeReshape/gradients/Cost/L2_regularization/add_1_grad/Sum1gradients/Cost/L2_regularization/add_1_grad/Shape*
T0*
Tshape0*
_output_shapes
: 

1gradients/Cost/L2_regularization/add_1_grad/Sum_1SumDgradients/Cost/L2_regularization/add_2_grad/tuple/control_dependencyCgradients/Cost/L2_regularization/add_1_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
×
5gradients/Cost/L2_regularization/add_1_grad/Reshape_1Reshape1gradients/Cost/L2_regularization/add_1_grad/Sum_13gradients/Cost/L2_regularization/add_1_grad/Shape_1*
_output_shapes
: *
T0*
Tshape0
˛
<gradients/Cost/L2_regularization/add_1_grad/tuple/group_depsNoOp4^gradients/Cost/L2_regularization/add_1_grad/Reshape6^gradients/Cost/L2_regularization/add_1_grad/Reshape_1
­
Dgradients/Cost/L2_regularization/add_1_grad/tuple/control_dependencyIdentity3gradients/Cost/L2_regularization/add_1_grad/Reshape=^gradients/Cost/L2_regularization/add_1_grad/tuple/group_deps*
_output_shapes
: *
T0*F
_class<
:8loc:@gradients/Cost/L2_regularization/add_1_grad/Reshape
ł
Fgradients/Cost/L2_regularization/add_1_grad/tuple/control_dependency_1Identity5gradients/Cost/L2_regularization/add_1_grad/Reshape_1=^gradients/Cost/L2_regularization/add_1_grad/tuple/group_deps*
T0*H
_class>
<:loc:@gradients/Cost/L2_regularization/add_1_grad/Reshape_1*
_output_shapes
: 
ˇ
2gradients/Cost/L2_regularization/L2Loss_3_grad/mulMul
FC1/W/readFgradients/Cost/L2_regularization/add_2_grad/tuple/control_dependency_1*
_output_shapes
:	@*
T0
r
/gradients/Cost/L2_regularization/add_grad/ShapeConst*
valueB *
dtype0*
_output_shapes
: 
t
1gradients/Cost/L2_regularization/add_grad/Shape_1Const*
valueB *
dtype0*
_output_shapes
: 
ů
?gradients/Cost/L2_regularization/add_grad/BroadcastGradientArgsBroadcastGradientArgs/gradients/Cost/L2_regularization/add_grad/Shape1gradients/Cost/L2_regularization/add_grad/Shape_1*
T0*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙
ű
-gradients/Cost/L2_regularization/add_grad/SumSumDgradients/Cost/L2_regularization/add_1_grad/tuple/control_dependency?gradients/Cost/L2_regularization/add_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Ë
1gradients/Cost/L2_regularization/add_grad/ReshapeReshape-gradients/Cost/L2_regularization/add_grad/Sum/gradients/Cost/L2_regularization/add_grad/Shape*
T0*
Tshape0*
_output_shapes
: 
˙
/gradients/Cost/L2_regularization/add_grad/Sum_1SumDgradients/Cost/L2_regularization/add_1_grad/tuple/control_dependencyAgradients/Cost/L2_regularization/add_grad/BroadcastGradientArgs:1*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
Ń
3gradients/Cost/L2_regularization/add_grad/Reshape_1Reshape/gradients/Cost/L2_regularization/add_grad/Sum_11gradients/Cost/L2_regularization/add_grad/Shape_1*
T0*
Tshape0*
_output_shapes
: 
Ź
:gradients/Cost/L2_regularization/add_grad/tuple/group_depsNoOp2^gradients/Cost/L2_regularization/add_grad/Reshape4^gradients/Cost/L2_regularization/add_grad/Reshape_1
Ľ
Bgradients/Cost/L2_regularization/add_grad/tuple/control_dependencyIdentity1gradients/Cost/L2_regularization/add_grad/Reshape;^gradients/Cost/L2_regularization/add_grad/tuple/group_deps*
T0*D
_class:
86loc:@gradients/Cost/L2_regularization/add_grad/Reshape*
_output_shapes
: 
Ť
Dgradients/Cost/L2_regularization/add_grad/tuple/control_dependency_1Identity3gradients/Cost/L2_regularization/add_grad/Reshape_1;^gradients/Cost/L2_regularization/add_grad/tuple/group_deps*
_output_shapes
: *
T0*F
_class<
:8loc:@gradients/Cost/L2_regularization/add_grad/Reshape_1
Ŕ
2gradients/Cost/L2_regularization/L2Loss_2_grad/mulMulConv3/W/readFgradients/Cost/L2_regularization/add_1_grad/tuple/control_dependency_1*&
_output_shapes
:00*
T0
ş
0gradients/Cost/L2_regularization/L2Loss_grad/mulMulConv1/W/readBgradients/Cost/L2_regularization/add_grad/tuple/control_dependency*&
_output_shapes
:*
T0
ž
2gradients/Cost/L2_regularization/L2Loss_1_grad/mulMulConv2/W/readDgradients/Cost/L2_regularization/add_grad/tuple/control_dependency_1*&
_output_shapes
:0*
T0
f
gradients/FC2/add_grad/ShapeShape
FC2/MatMul*
_output_shapes
:*
T0*
out_type0
h
gradients/FC2/add_grad/Shape_1Const*
valueB:
*
dtype0*
_output_shapes
:
Ŕ
,gradients/FC2/add_grad/BroadcastGradientArgsBroadcastGradientArgsgradients/FC2/add_grad/Shapegradients/FC2/add_grad/Shape_1*
T0*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙
Â
gradients/FC2/add_grad/SumSum1gradients/Cost/cross_entropy/Reshape_grad/Reshape,gradients/FC2/add_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Ł
gradients/FC2/add_grad/ReshapeReshapegradients/FC2/add_grad/Sumgradients/FC2/add_grad/Shape*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
*
T0*
Tshape0
Ć
gradients/FC2/add_grad/Sum_1Sum1gradients/Cost/cross_entropy/Reshape_grad/Reshape.gradients/FC2/add_grad/BroadcastGradientArgs:1*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0

 gradients/FC2/add_grad/Reshape_1Reshapegradients/FC2/add_grad/Sum_1gradients/FC2/add_grad/Shape_1*
_output_shapes
:
*
T0*
Tshape0
s
'gradients/FC2/add_grad/tuple/group_depsNoOp^gradients/FC2/add_grad/Reshape!^gradients/FC2/add_grad/Reshape_1
ę
/gradients/FC2/add_grad/tuple/control_dependencyIdentitygradients/FC2/add_grad/Reshape(^gradients/FC2/add_grad/tuple/group_deps*1
_class'
%#loc:@gradients/FC2/add_grad/Reshape*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
*
T0
ă
1gradients/FC2/add_grad/tuple/control_dependency_1Identity gradients/FC2/add_grad/Reshape_1(^gradients/FC2/add_grad/tuple/group_deps*3
_class)
'%loc:@gradients/FC2/add_grad/Reshape_1*
_output_shapes
:
*
T0
ż
 gradients/FC2/MatMul_grad/MatMulMatMul/gradients/FC2/add_grad/tuple/control_dependency
FC2/W/read*
T0*'
_output_shapes
:˙˙˙˙˙˙˙˙˙@*
transpose_a( *
transpose_b(
˝
"gradients/FC2/MatMul_grad/MatMul_1MatMulFC2/dropout/mul/gradients/FC2/add_grad/tuple/control_dependency*
T0*
_output_shapes

:@
*
transpose_a(*
transpose_b( 
z
*gradients/FC2/MatMul_grad/tuple/group_depsNoOp!^gradients/FC2/MatMul_grad/MatMul#^gradients/FC2/MatMul_grad/MatMul_1
ô
2gradients/FC2/MatMul_grad/tuple/control_dependencyIdentity gradients/FC2/MatMul_grad/MatMul+^gradients/FC2/MatMul_grad/tuple/group_deps*
T0*3
_class)
'%loc:@gradients/FC2/MatMul_grad/MatMul*'
_output_shapes
:˙˙˙˙˙˙˙˙˙@
ń
4gradients/FC2/MatMul_grad/tuple/control_dependency_1Identity"gradients/FC2/MatMul_grad/MatMul_1+^gradients/FC2/MatMul_grad/tuple/group_deps*
T0*5
_class+
)'loc:@gradients/FC2/MatMul_grad/MatMul_1*
_output_shapes

:@

|
$gradients/FC2/dropout/mul_grad/ShapeShapeFC2/dropout/div*
out_type0*#
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0

&gradients/FC2/dropout/mul_grad/Shape_1ShapeFC2/dropout/Floor*
T0*
out_type0*#
_output_shapes
:˙˙˙˙˙˙˙˙˙
Ř
4gradients/FC2/dropout/mul_grad/BroadcastGradientArgsBroadcastGradientArgs$gradients/FC2/dropout/mul_grad/Shape&gradients/FC2/dropout/mul_grad/Shape_1*
T0*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙

"gradients/FC2/dropout/mul_grad/mulMul2gradients/FC2/MatMul_grad/tuple/control_dependencyFC2/dropout/Floor*
T0*
_output_shapes
:
Ă
"gradients/FC2/dropout/mul_grad/SumSum"gradients/FC2/dropout/mul_grad/mul4gradients/FC2/dropout/mul_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Ź
&gradients/FC2/dropout/mul_grad/ReshapeReshape"gradients/FC2/dropout/mul_grad/Sum$gradients/FC2/dropout/mul_grad/Shape*
_output_shapes
:*
T0*
Tshape0

$gradients/FC2/dropout/mul_grad/mul_1MulFC2/dropout/div2gradients/FC2/MatMul_grad/tuple/control_dependency*
_output_shapes
:*
T0
É
$gradients/FC2/dropout/mul_grad/Sum_1Sum$gradients/FC2/dropout/mul_grad/mul_16gradients/FC2/dropout/mul_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
˛
(gradients/FC2/dropout/mul_grad/Reshape_1Reshape$gradients/FC2/dropout/mul_grad/Sum_1&gradients/FC2/dropout/mul_grad/Shape_1*
_output_shapes
:*
T0*
Tshape0

/gradients/FC2/dropout/mul_grad/tuple/group_depsNoOp'^gradients/FC2/dropout/mul_grad/Reshape)^gradients/FC2/dropout/mul_grad/Reshape_1
ű
7gradients/FC2/dropout/mul_grad/tuple/control_dependencyIdentity&gradients/FC2/dropout/mul_grad/Reshape0^gradients/FC2/dropout/mul_grad/tuple/group_deps*
_output_shapes
:*
T0*9
_class/
-+loc:@gradients/FC2/dropout/mul_grad/Reshape

9gradients/FC2/dropout/mul_grad/tuple/control_dependency_1Identity(gradients/FC2/dropout/mul_grad/Reshape_10^gradients/FC2/dropout/mul_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients/FC2/dropout/mul_grad/Reshape_1*
_output_shapes
:
ů
gradients/AddNAddN2gradients/Cost/L2_regularization/L2Loss_4_grad/mul4gradients/FC2/MatMul_grad/tuple/control_dependency_1*E
_class;
97loc:@gradients/Cost/L2_regularization/L2Loss_4_grad/mul*
N*
_output_shapes

:@
*
T0
l
$gradients/FC2/dropout/div_grad/ShapeShapeFC1/Relu*
T0*
out_type0*
_output_shapes
:

&gradients/FC2/dropout/div_grad/Shape_1Shapekeep_prob/Placeholder*#
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0*
out_type0
Ř
4gradients/FC2/dropout/div_grad/BroadcastGradientArgsBroadcastGradientArgs$gradients/FC2/dropout/div_grad/Shape&gradients/FC2/dropout/div_grad/Shape_1*
T0*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙
¤
&gradients/FC2/dropout/div_grad/RealDivRealDiv7gradients/FC2/dropout/mul_grad/tuple/control_dependencykeep_prob/Placeholder*
T0*
_output_shapes
:
Ç
"gradients/FC2/dropout/div_grad/SumSum&gradients/FC2/dropout/div_grad/RealDiv4gradients/FC2/dropout/div_grad/BroadcastGradientArgs*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
ť
&gradients/FC2/dropout/div_grad/ReshapeReshape"gradients/FC2/dropout/div_grad/Sum$gradients/FC2/dropout/div_grad/Shape*'
_output_shapes
:˙˙˙˙˙˙˙˙˙@*
T0*
Tshape0
e
"gradients/FC2/dropout/div_grad/NegNegFC1/Relu*
T0*'
_output_shapes
:˙˙˙˙˙˙˙˙˙@

(gradients/FC2/dropout/div_grad/RealDiv_1RealDiv"gradients/FC2/dropout/div_grad/Negkeep_prob/Placeholder*
T0*
_output_shapes
:

(gradients/FC2/dropout/div_grad/RealDiv_2RealDiv(gradients/FC2/dropout/div_grad/RealDiv_1keep_prob/Placeholder*
_output_shapes
:*
T0
Ż
"gradients/FC2/dropout/div_grad/mulMul7gradients/FC2/dropout/mul_grad/tuple/control_dependency(gradients/FC2/dropout/div_grad/RealDiv_2*
T0*
_output_shapes
:
Ç
$gradients/FC2/dropout/div_grad/Sum_1Sum"gradients/FC2/dropout/div_grad/mul6gradients/FC2/dropout/div_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
˛
(gradients/FC2/dropout/div_grad/Reshape_1Reshape$gradients/FC2/dropout/div_grad/Sum_1&gradients/FC2/dropout/div_grad/Shape_1*
T0*
Tshape0*
_output_shapes
:

/gradients/FC2/dropout/div_grad/tuple/group_depsNoOp'^gradients/FC2/dropout/div_grad/Reshape)^gradients/FC2/dropout/div_grad/Reshape_1

7gradients/FC2/dropout/div_grad/tuple/control_dependencyIdentity&gradients/FC2/dropout/div_grad/Reshape0^gradients/FC2/dropout/div_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients/FC2/dropout/div_grad/Reshape*'
_output_shapes
:˙˙˙˙˙˙˙˙˙@

9gradients/FC2/dropout/div_grad/tuple/control_dependency_1Identity(gradients/FC2/dropout/div_grad/Reshape_10^gradients/FC2/dropout/div_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients/FC2/dropout/div_grad/Reshape_1*
_output_shapes
:
Ą
 gradients/FC1/Relu_grad/ReluGradReluGrad7gradients/FC2/dropout/div_grad/tuple/control_dependencyFC1/Relu*
T0*'
_output_shapes
:˙˙˙˙˙˙˙˙˙@
f
gradients/FC1/add_grad/ShapeShape
FC1/MatMul*
T0*
out_type0*
_output_shapes
:
h
gradients/FC1/add_grad/Shape_1Const*
valueB:@*
dtype0*
_output_shapes
:
Ŕ
,gradients/FC1/add_grad/BroadcastGradientArgsBroadcastGradientArgsgradients/FC1/add_grad/Shapegradients/FC1/add_grad/Shape_1*
T0*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙
ą
gradients/FC1/add_grad/SumSum gradients/FC1/Relu_grad/ReluGrad,gradients/FC1/add_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
Ł
gradients/FC1/add_grad/ReshapeReshapegradients/FC1/add_grad/Sumgradients/FC1/add_grad/Shape*
T0*
Tshape0*'
_output_shapes
:˙˙˙˙˙˙˙˙˙@
ľ
gradients/FC1/add_grad/Sum_1Sum gradients/FC1/Relu_grad/ReluGrad.gradients/FC1/add_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:

 gradients/FC1/add_grad/Reshape_1Reshapegradients/FC1/add_grad/Sum_1gradients/FC1/add_grad/Shape_1*
T0*
Tshape0*
_output_shapes
:@
s
'gradients/FC1/add_grad/tuple/group_depsNoOp^gradients/FC1/add_grad/Reshape!^gradients/FC1/add_grad/Reshape_1
ę
/gradients/FC1/add_grad/tuple/control_dependencyIdentitygradients/FC1/add_grad/Reshape(^gradients/FC1/add_grad/tuple/group_deps*'
_output_shapes
:˙˙˙˙˙˙˙˙˙@*
T0*1
_class'
%#loc:@gradients/FC1/add_grad/Reshape
ă
1gradients/FC1/add_grad/tuple/control_dependency_1Identity gradients/FC1/add_grad/Reshape_1(^gradients/FC1/add_grad/tuple/group_deps*
T0*3
_class)
'%loc:@gradients/FC1/add_grad/Reshape_1*
_output_shapes
:@
Ŕ
 gradients/FC1/MatMul_grad/MatMulMatMul/gradients/FC1/add_grad/tuple/control_dependency
FC1/W/read*
T0*(
_output_shapes
:˙˙˙˙˙˙˙˙˙*
transpose_a( *
transpose_b(
ž
"gradients/FC1/MatMul_grad/MatMul_1MatMulFC1/dropout/mul/gradients/FC1/add_grad/tuple/control_dependency*
transpose_b( *
T0*
_output_shapes
:	@*
transpose_a(
z
*gradients/FC1/MatMul_grad/tuple/group_depsNoOp!^gradients/FC1/MatMul_grad/MatMul#^gradients/FC1/MatMul_grad/MatMul_1
ő
2gradients/FC1/MatMul_grad/tuple/control_dependencyIdentity gradients/FC1/MatMul_grad/MatMul+^gradients/FC1/MatMul_grad/tuple/group_deps*
T0*3
_class)
'%loc:@gradients/FC1/MatMul_grad/MatMul*(
_output_shapes
:˙˙˙˙˙˙˙˙˙
ň
4gradients/FC1/MatMul_grad/tuple/control_dependency_1Identity"gradients/FC1/MatMul_grad/MatMul_1+^gradients/FC1/MatMul_grad/tuple/group_deps*5
_class+
)'loc:@gradients/FC1/MatMul_grad/MatMul_1*
_output_shapes
:	@*
T0
|
$gradients/FC1/dropout/mul_grad/ShapeShapeFC1/dropout/div*#
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0*
out_type0

&gradients/FC1/dropout/mul_grad/Shape_1ShapeFC1/dropout/Floor*
T0*
out_type0*#
_output_shapes
:˙˙˙˙˙˙˙˙˙
Ř
4gradients/FC1/dropout/mul_grad/BroadcastGradientArgsBroadcastGradientArgs$gradients/FC1/dropout/mul_grad/Shape&gradients/FC1/dropout/mul_grad/Shape_1*
T0*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙

"gradients/FC1/dropout/mul_grad/mulMul2gradients/FC1/MatMul_grad/tuple/control_dependencyFC1/dropout/Floor*
T0*
_output_shapes
:
Ă
"gradients/FC1/dropout/mul_grad/SumSum"gradients/FC1/dropout/mul_grad/mul4gradients/FC1/dropout/mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
Ź
&gradients/FC1/dropout/mul_grad/ReshapeReshape"gradients/FC1/dropout/mul_grad/Sum$gradients/FC1/dropout/mul_grad/Shape*
Tshape0*
_output_shapes
:*
T0

$gradients/FC1/dropout/mul_grad/mul_1MulFC1/dropout/div2gradients/FC1/MatMul_grad/tuple/control_dependency*
T0*
_output_shapes
:
É
$gradients/FC1/dropout/mul_grad/Sum_1Sum$gradients/FC1/dropout/mul_grad/mul_16gradients/FC1/dropout/mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
˛
(gradients/FC1/dropout/mul_grad/Reshape_1Reshape$gradients/FC1/dropout/mul_grad/Sum_1&gradients/FC1/dropout/mul_grad/Shape_1*
T0*
Tshape0*
_output_shapes
:

/gradients/FC1/dropout/mul_grad/tuple/group_depsNoOp'^gradients/FC1/dropout/mul_grad/Reshape)^gradients/FC1/dropout/mul_grad/Reshape_1
ű
7gradients/FC1/dropout/mul_grad/tuple/control_dependencyIdentity&gradients/FC1/dropout/mul_grad/Reshape0^gradients/FC1/dropout/mul_grad/tuple/group_deps*9
_class/
-+loc:@gradients/FC1/dropout/mul_grad/Reshape*
_output_shapes
:*
T0

9gradients/FC1/dropout/mul_grad/tuple/control_dependency_1Identity(gradients/FC1/dropout/mul_grad/Reshape_10^gradients/FC1/dropout/mul_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients/FC1/dropout/mul_grad/Reshape_1*
_output_shapes
:
ü
gradients/AddN_1AddN2gradients/Cost/L2_regularization/L2Loss_3_grad/mul4gradients/FC1/MatMul_grad/tuple/control_dependency_1*E
_class;
97loc:@gradients/Cost/L2_regularization/L2Loss_3_grad/mul*
N*
_output_shapes
:	@*
T0
s
$gradients/FC1/dropout/div_grad/ShapeShapeFlatten/Reshape*
T0*
out_type0*
_output_shapes
:

&gradients/FC1/dropout/div_grad/Shape_1Shapekeep_prob/Placeholder*
T0*
out_type0*#
_output_shapes
:˙˙˙˙˙˙˙˙˙
Ř
4gradients/FC1/dropout/div_grad/BroadcastGradientArgsBroadcastGradientArgs$gradients/FC1/dropout/div_grad/Shape&gradients/FC1/dropout/div_grad/Shape_1*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙*
T0
¤
&gradients/FC1/dropout/div_grad/RealDivRealDiv7gradients/FC1/dropout/mul_grad/tuple/control_dependencykeep_prob/Placeholder*
T0*
_output_shapes
:
Ç
"gradients/FC1/dropout/div_grad/SumSum&gradients/FC1/dropout/div_grad/RealDiv4gradients/FC1/dropout/div_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
ź
&gradients/FC1/dropout/div_grad/ReshapeReshape"gradients/FC1/dropout/div_grad/Sum$gradients/FC1/dropout/div_grad/Shape*(
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0*
Tshape0
m
"gradients/FC1/dropout/div_grad/NegNegFlatten/Reshape*(
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0

(gradients/FC1/dropout/div_grad/RealDiv_1RealDiv"gradients/FC1/dropout/div_grad/Negkeep_prob/Placeholder*
T0*
_output_shapes
:

(gradients/FC1/dropout/div_grad/RealDiv_2RealDiv(gradients/FC1/dropout/div_grad/RealDiv_1keep_prob/Placeholder*
_output_shapes
:*
T0
Ż
"gradients/FC1/dropout/div_grad/mulMul7gradients/FC1/dropout/mul_grad/tuple/control_dependency(gradients/FC1/dropout/div_grad/RealDiv_2*
T0*
_output_shapes
:
Ç
$gradients/FC1/dropout/div_grad/Sum_1Sum"gradients/FC1/dropout/div_grad/mul6gradients/FC1/dropout/div_grad/BroadcastGradientArgs:1*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
˛
(gradients/FC1/dropout/div_grad/Reshape_1Reshape$gradients/FC1/dropout/div_grad/Sum_1&gradients/FC1/dropout/div_grad/Shape_1*
T0*
Tshape0*
_output_shapes
:

/gradients/FC1/dropout/div_grad/tuple/group_depsNoOp'^gradients/FC1/dropout/div_grad/Reshape)^gradients/FC1/dropout/div_grad/Reshape_1

7gradients/FC1/dropout/div_grad/tuple/control_dependencyIdentity&gradients/FC1/dropout/div_grad/Reshape0^gradients/FC1/dropout/div_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients/FC1/dropout/div_grad/Reshape*(
_output_shapes
:˙˙˙˙˙˙˙˙˙

9gradients/FC1/dropout/div_grad/tuple/control_dependency_1Identity(gradients/FC1/dropout/div_grad/Reshape_10^gradients/FC1/dropout/div_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients/FC1/dropout/div_grad/Reshape_1*
_output_shapes
:
q
$gradients/Flatten/Reshape_grad/ShapeShapeConv3/MaxPool*
T0*
out_type0*
_output_shapes
:
Ř
&gradients/Flatten/Reshape_grad/ReshapeReshape7gradients/FC1/dropout/div_grad/tuple/control_dependency$gradients/Flatten/Reshape_grad/Shape*
T0*
Tshape0*/
_output_shapes
:˙˙˙˙˙˙˙˙˙0

(gradients/Conv3/MaxPool_grad/MaxPoolGradMaxPoolGrad
Conv3/ReluConv3/MaxPool&gradients/Flatten/Reshape_grad/Reshape*/
_output_shapes
:˙˙˙˙˙˙˙˙˙0*
T0*
strides
*
data_formatNHWC*
ksize
*
paddingVALID

"gradients/Conv3/Relu_grad/ReluGradReluGrad(gradients/Conv3/MaxPool_grad/MaxPoolGrad
Conv3/Relu*/
_output_shapes
:˙˙˙˙˙˙˙˙˙0*
T0

(gradients/Conv3/BiasAdd_grad/BiasAddGradBiasAddGrad"gradients/Conv3/Relu_grad/ReluGrad*
T0*
data_formatNHWC*
_output_shapes
:0

-gradients/Conv3/BiasAdd_grad/tuple/group_depsNoOp#^gradients/Conv3/Relu_grad/ReluGrad)^gradients/Conv3/BiasAdd_grad/BiasAddGrad

5gradients/Conv3/BiasAdd_grad/tuple/control_dependencyIdentity"gradients/Conv3/Relu_grad/ReluGrad.^gradients/Conv3/BiasAdd_grad/tuple/group_deps*/
_output_shapes
:˙˙˙˙˙˙˙˙˙0*
T0*5
_class+
)'loc:@gradients/Conv3/Relu_grad/ReluGrad
˙
7gradients/Conv3/BiasAdd_grad/tuple/control_dependency_1Identity(gradients/Conv3/BiasAdd_grad/BiasAddGrad.^gradients/Conv3/BiasAdd_grad/tuple/group_deps*
_output_shapes
:0*
T0*;
_class1
/-loc:@gradients/Conv3/BiasAdd_grad/BiasAddGrad
n
!gradients/Conv3/Conv2D_grad/ShapeShapeConv2/MaxPool*
T0*
out_type0*
_output_shapes
:
ŕ
/gradients/Conv3/Conv2D_grad/Conv2DBackpropInputConv2DBackpropInput!gradients/Conv3/Conv2D_grad/ShapeConv3/dropout/mul5gradients/Conv3/BiasAdd_grad/tuple/control_dependency*
strides
*
data_formatNHWC*
use_cudnn_on_gpu(*
paddingVALID*J
_output_shapes8
6:4˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
|
#gradients/Conv3/Conv2D_grad/Shape_1Const*
dtype0*
_output_shapes
:*%
valueB"      0   0   
ź
0gradients/Conv3/Conv2D_grad/Conv2DBackpropFilterConv2DBackpropFilterConv2/MaxPool#gradients/Conv3/Conv2D_grad/Shape_15gradients/Conv3/BiasAdd_grad/tuple/control_dependency*
paddingVALID*&
_output_shapes
:00*
T0*
strides
*
data_formatNHWC*
use_cudnn_on_gpu(

,gradients/Conv3/Conv2D_grad/tuple/group_depsNoOp0^gradients/Conv3/Conv2D_grad/Conv2DBackpropInput1^gradients/Conv3/Conv2D_grad/Conv2DBackpropFilter

4gradients/Conv3/Conv2D_grad/tuple/control_dependencyIdentity/gradients/Conv3/Conv2D_grad/Conv2DBackpropInput-^gradients/Conv3/Conv2D_grad/tuple/group_deps*/
_output_shapes
:˙˙˙˙˙˙˙˙˙0*
T0*B
_class8
64loc:@gradients/Conv3/Conv2D_grad/Conv2DBackpropInput

6gradients/Conv3/Conv2D_grad/tuple/control_dependency_1Identity0gradients/Conv3/Conv2D_grad/Conv2DBackpropFilter-^gradients/Conv3/Conv2D_grad/tuple/group_deps*
T0*C
_class9
75loc:@gradients/Conv3/Conv2D_grad/Conv2DBackpropFilter*&
_output_shapes
:00

(gradients/Conv2/MaxPool_grad/MaxPoolGradMaxPoolGrad
Conv2/ReluConv2/MaxPool4gradients/Conv3/Conv2D_grad/tuple/control_dependency*/
_output_shapes
:˙˙˙˙˙˙˙˙˙;0*
T0*
strides
*
data_formatNHWC*
ksize
*
paddingVALID

&gradients/Conv3/dropout/mul_grad/ShapeShapeConv3/dropout/div*#
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0*
out_type0

(gradients/Conv3/dropout/mul_grad/Shape_1ShapeConv3/dropout/Floor*
T0*
out_type0*#
_output_shapes
:˙˙˙˙˙˙˙˙˙
Ţ
6gradients/Conv3/dropout/mul_grad/BroadcastGradientArgsBroadcastGradientArgs&gradients/Conv3/dropout/mul_grad/Shape(gradients/Conv3/dropout/mul_grad/Shape_1*
T0*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙

$gradients/Conv3/dropout/mul_grad/mulMul6gradients/Conv3/Conv2D_grad/tuple/control_dependency_1Conv3/dropout/Floor*
T0*
_output_shapes
:
É
$gradients/Conv3/dropout/mul_grad/SumSum$gradients/Conv3/dropout/mul_grad/mul6gradients/Conv3/dropout/mul_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
˛
(gradients/Conv3/dropout/mul_grad/ReshapeReshape$gradients/Conv3/dropout/mul_grad/Sum&gradients/Conv3/dropout/mul_grad/Shape*
_output_shapes
:*
T0*
Tshape0

&gradients/Conv3/dropout/mul_grad/mul_1MulConv3/dropout/div6gradients/Conv3/Conv2D_grad/tuple/control_dependency_1*
T0*
_output_shapes
:
Ď
&gradients/Conv3/dropout/mul_grad/Sum_1Sum&gradients/Conv3/dropout/mul_grad/mul_18gradients/Conv3/dropout/mul_grad/BroadcastGradientArgs:1*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
¸
*gradients/Conv3/dropout/mul_grad/Reshape_1Reshape&gradients/Conv3/dropout/mul_grad/Sum_1(gradients/Conv3/dropout/mul_grad/Shape_1*
_output_shapes
:*
T0*
Tshape0

1gradients/Conv3/dropout/mul_grad/tuple/group_depsNoOp)^gradients/Conv3/dropout/mul_grad/Reshape+^gradients/Conv3/dropout/mul_grad/Reshape_1

9gradients/Conv3/dropout/mul_grad/tuple/control_dependencyIdentity(gradients/Conv3/dropout/mul_grad/Reshape2^gradients/Conv3/dropout/mul_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients/Conv3/dropout/mul_grad/Reshape*
_output_shapes
:

;gradients/Conv3/dropout/mul_grad/tuple/control_dependency_1Identity*gradients/Conv3/dropout/mul_grad/Reshape_12^gradients/Conv3/dropout/mul_grad/tuple/group_deps*
T0*=
_class3
1/loc:@gradients/Conv3/dropout/mul_grad/Reshape_1*
_output_shapes
:

"gradients/Conv2/Relu_grad/ReluGradReluGrad(gradients/Conv2/MaxPool_grad/MaxPoolGrad
Conv2/Relu*
T0*/
_output_shapes
:˙˙˙˙˙˙˙˙˙;0

&gradients/Conv3/dropout/div_grad/ShapeConst*
dtype0*
_output_shapes
:*%
valueB"      0   0   

(gradients/Conv3/dropout/div_grad/Shape_1Shapekeep_prob_conv/Placeholder*#
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0*
out_type0
Ţ
6gradients/Conv3/dropout/div_grad/BroadcastGradientArgsBroadcastGradientArgs&gradients/Conv3/dropout/div_grad/Shape(gradients/Conv3/dropout/div_grad/Shape_1*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙*
T0
­
(gradients/Conv3/dropout/div_grad/RealDivRealDiv9gradients/Conv3/dropout/mul_grad/tuple/control_dependencykeep_prob_conv/Placeholder*
T0*
_output_shapes
:
Í
$gradients/Conv3/dropout/div_grad/SumSum(gradients/Conv3/dropout/div_grad/RealDiv6gradients/Conv3/dropout/div_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Ŕ
(gradients/Conv3/dropout/div_grad/ReshapeReshape$gradients/Conv3/dropout/div_grad/Sum&gradients/Conv3/dropout/div_grad/Shape*
Tshape0*&
_output_shapes
:00*
T0
j
$gradients/Conv3/dropout/div_grad/NegNegConv3/W/read*&
_output_shapes
:00*
T0

*gradients/Conv3/dropout/div_grad/RealDiv_1RealDiv$gradients/Conv3/dropout/div_grad/Negkeep_prob_conv/Placeholder*
T0*
_output_shapes
:
 
*gradients/Conv3/dropout/div_grad/RealDiv_2RealDiv*gradients/Conv3/dropout/div_grad/RealDiv_1keep_prob_conv/Placeholder*
T0*
_output_shapes
:
ľ
$gradients/Conv3/dropout/div_grad/mulMul9gradients/Conv3/dropout/mul_grad/tuple/control_dependency*gradients/Conv3/dropout/div_grad/RealDiv_2*
T0*
_output_shapes
:
Í
&gradients/Conv3/dropout/div_grad/Sum_1Sum$gradients/Conv3/dropout/div_grad/mul8gradients/Conv3/dropout/div_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:
¸
*gradients/Conv3/dropout/div_grad/Reshape_1Reshape&gradients/Conv3/dropout/div_grad/Sum_1(gradients/Conv3/dropout/div_grad/Shape_1*
T0*
Tshape0*
_output_shapes
:

1gradients/Conv3/dropout/div_grad/tuple/group_depsNoOp)^gradients/Conv3/dropout/div_grad/Reshape+^gradients/Conv3/dropout/div_grad/Reshape_1

9gradients/Conv3/dropout/div_grad/tuple/control_dependencyIdentity(gradients/Conv3/dropout/div_grad/Reshape2^gradients/Conv3/dropout/div_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients/Conv3/dropout/div_grad/Reshape*&
_output_shapes
:00

;gradients/Conv3/dropout/div_grad/tuple/control_dependency_1Identity*gradients/Conv3/dropout/div_grad/Reshape_12^gradients/Conv3/dropout/div_grad/tuple/group_deps*
_output_shapes
:*
T0*=
_class3
1/loc:@gradients/Conv3/dropout/div_grad/Reshape_1

(gradients/Conv2/BiasAdd_grad/BiasAddGradBiasAddGrad"gradients/Conv2/Relu_grad/ReluGrad*
T0*
data_formatNHWC*
_output_shapes
:0

-gradients/Conv2/BiasAdd_grad/tuple/group_depsNoOp#^gradients/Conv2/Relu_grad/ReluGrad)^gradients/Conv2/BiasAdd_grad/BiasAddGrad

5gradients/Conv2/BiasAdd_grad/tuple/control_dependencyIdentity"gradients/Conv2/Relu_grad/ReluGrad.^gradients/Conv2/BiasAdd_grad/tuple/group_deps*/
_output_shapes
:˙˙˙˙˙˙˙˙˙;0*
T0*5
_class+
)'loc:@gradients/Conv2/Relu_grad/ReluGrad
˙
7gradients/Conv2/BiasAdd_grad/tuple/control_dependency_1Identity(gradients/Conv2/BiasAdd_grad/BiasAddGrad.^gradients/Conv2/BiasAdd_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients/Conv2/BiasAdd_grad/BiasAddGrad*
_output_shapes
:0

gradients/AddN_2AddN2gradients/Cost/L2_regularization/L2Loss_2_grad/mul9gradients/Conv3/dropout/div_grad/tuple/control_dependency*&
_output_shapes
:00*
T0*E
_class;
97loc:@gradients/Cost/L2_regularization/L2Loss_2_grad/mul*
N
n
!gradients/Conv2/Conv2D_grad/ShapeShapeConv1/MaxPool*
T0*
out_type0*
_output_shapes
:
ŕ
/gradients/Conv2/Conv2D_grad/Conv2DBackpropInputConv2DBackpropInput!gradients/Conv2/Conv2D_grad/ShapeConv2/dropout/mul5gradients/Conv2/BiasAdd_grad/tuple/control_dependency*
paddingVALID*J
_output_shapes8
6:4˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0*
strides
*
data_formatNHWC*
use_cudnn_on_gpu(
|
#gradients/Conv2/Conv2D_grad/Shape_1Const*%
valueB"         0   *
dtype0*
_output_shapes
:
ź
0gradients/Conv2/Conv2D_grad/Conv2DBackpropFilterConv2DBackpropFilterConv1/MaxPool#gradients/Conv2/Conv2D_grad/Shape_15gradients/Conv2/BiasAdd_grad/tuple/control_dependency*&
_output_shapes
:0*
T0*
strides
*
data_formatNHWC*
use_cudnn_on_gpu(*
paddingVALID

,gradients/Conv2/Conv2D_grad/tuple/group_depsNoOp0^gradients/Conv2/Conv2D_grad/Conv2DBackpropInput1^gradients/Conv2/Conv2D_grad/Conv2DBackpropFilter

4gradients/Conv2/Conv2D_grad/tuple/control_dependencyIdentity/gradients/Conv2/Conv2D_grad/Conv2DBackpropInput-^gradients/Conv2/Conv2D_grad/tuple/group_deps*/
_output_shapes
:˙˙˙˙˙˙˙˙˙?*
T0*B
_class8
64loc:@gradients/Conv2/Conv2D_grad/Conv2DBackpropInput

6gradients/Conv2/Conv2D_grad/tuple/control_dependency_1Identity0gradients/Conv2/Conv2D_grad/Conv2DBackpropFilter-^gradients/Conv2/Conv2D_grad/tuple/group_deps*C
_class9
75loc:@gradients/Conv2/Conv2D_grad/Conv2DBackpropFilter*&
_output_shapes
:0*
T0

(gradients/Conv1/MaxPool_grad/MaxPoolGradMaxPoolGrad
Conv1/ReluConv1/MaxPool4gradients/Conv2/Conv2D_grad/tuple/control_dependency*
T0*
strides
*
data_formatNHWC*
ksize
*
paddingVALID*/
_output_shapes
:˙˙˙˙˙˙˙˙˙8~

&gradients/Conv2/dropout/mul_grad/ShapeShapeConv2/dropout/div*#
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0*
out_type0

(gradients/Conv2/dropout/mul_grad/Shape_1ShapeConv2/dropout/Floor*
T0*
out_type0*#
_output_shapes
:˙˙˙˙˙˙˙˙˙
Ţ
6gradients/Conv2/dropout/mul_grad/BroadcastGradientArgsBroadcastGradientArgs&gradients/Conv2/dropout/mul_grad/Shape(gradients/Conv2/dropout/mul_grad/Shape_1*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙*
T0

$gradients/Conv2/dropout/mul_grad/mulMul6gradients/Conv2/Conv2D_grad/tuple/control_dependency_1Conv2/dropout/Floor*
T0*
_output_shapes
:
É
$gradients/Conv2/dropout/mul_grad/SumSum$gradients/Conv2/dropout/mul_grad/mul6gradients/Conv2/dropout/mul_grad/BroadcastGradientArgs*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0
˛
(gradients/Conv2/dropout/mul_grad/ReshapeReshape$gradients/Conv2/dropout/mul_grad/Sum&gradients/Conv2/dropout/mul_grad/Shape*
T0*
Tshape0*
_output_shapes
:

&gradients/Conv2/dropout/mul_grad/mul_1MulConv2/dropout/div6gradients/Conv2/Conv2D_grad/tuple/control_dependency_1*
T0*
_output_shapes
:
Ď
&gradients/Conv2/dropout/mul_grad/Sum_1Sum&gradients/Conv2/dropout/mul_grad/mul_18gradients/Conv2/dropout/mul_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
¸
*gradients/Conv2/dropout/mul_grad/Reshape_1Reshape&gradients/Conv2/dropout/mul_grad/Sum_1(gradients/Conv2/dropout/mul_grad/Shape_1*
T0*
Tshape0*
_output_shapes
:

1gradients/Conv2/dropout/mul_grad/tuple/group_depsNoOp)^gradients/Conv2/dropout/mul_grad/Reshape+^gradients/Conv2/dropout/mul_grad/Reshape_1

9gradients/Conv2/dropout/mul_grad/tuple/control_dependencyIdentity(gradients/Conv2/dropout/mul_grad/Reshape2^gradients/Conv2/dropout/mul_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients/Conv2/dropout/mul_grad/Reshape*
_output_shapes
:

;gradients/Conv2/dropout/mul_grad/tuple/control_dependency_1Identity*gradients/Conv2/dropout/mul_grad/Reshape_12^gradients/Conv2/dropout/mul_grad/tuple/group_deps*
T0*=
_class3
1/loc:@gradients/Conv2/dropout/mul_grad/Reshape_1*
_output_shapes
:

"gradients/Conv1/Relu_grad/ReluGradReluGrad(gradients/Conv1/MaxPool_grad/MaxPoolGrad
Conv1/Relu*/
_output_shapes
:˙˙˙˙˙˙˙˙˙8~*
T0

&gradients/Conv2/dropout/div_grad/ShapeConst*%
valueB"         0   *
dtype0*
_output_shapes
:

(gradients/Conv2/dropout/div_grad/Shape_1Shapekeep_prob_conv/Placeholder*#
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0*
out_type0
Ţ
6gradients/Conv2/dropout/div_grad/BroadcastGradientArgsBroadcastGradientArgs&gradients/Conv2/dropout/div_grad/Shape(gradients/Conv2/dropout/div_grad/Shape_1*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙*
T0
­
(gradients/Conv2/dropout/div_grad/RealDivRealDiv9gradients/Conv2/dropout/mul_grad/tuple/control_dependencykeep_prob_conv/Placeholder*
_output_shapes
:*
T0
Í
$gradients/Conv2/dropout/div_grad/SumSum(gradients/Conv2/dropout/div_grad/RealDiv6gradients/Conv2/dropout/div_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Ŕ
(gradients/Conv2/dropout/div_grad/ReshapeReshape$gradients/Conv2/dropout/div_grad/Sum&gradients/Conv2/dropout/div_grad/Shape*
T0*
Tshape0*&
_output_shapes
:0
j
$gradients/Conv2/dropout/div_grad/NegNegConv2/W/read*&
_output_shapes
:0*
T0

*gradients/Conv2/dropout/div_grad/RealDiv_1RealDiv$gradients/Conv2/dropout/div_grad/Negkeep_prob_conv/Placeholder*
_output_shapes
:*
T0
 
*gradients/Conv2/dropout/div_grad/RealDiv_2RealDiv*gradients/Conv2/dropout/div_grad/RealDiv_1keep_prob_conv/Placeholder*
_output_shapes
:*
T0
ľ
$gradients/Conv2/dropout/div_grad/mulMul9gradients/Conv2/dropout/mul_grad/tuple/control_dependency*gradients/Conv2/dropout/div_grad/RealDiv_2*
_output_shapes
:*
T0
Í
&gradients/Conv2/dropout/div_grad/Sum_1Sum$gradients/Conv2/dropout/div_grad/mul8gradients/Conv2/dropout/div_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
¸
*gradients/Conv2/dropout/div_grad/Reshape_1Reshape&gradients/Conv2/dropout/div_grad/Sum_1(gradients/Conv2/dropout/div_grad/Shape_1*
_output_shapes
:*
T0*
Tshape0

1gradients/Conv2/dropout/div_grad/tuple/group_depsNoOp)^gradients/Conv2/dropout/div_grad/Reshape+^gradients/Conv2/dropout/div_grad/Reshape_1

9gradients/Conv2/dropout/div_grad/tuple/control_dependencyIdentity(gradients/Conv2/dropout/div_grad/Reshape2^gradients/Conv2/dropout/div_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients/Conv2/dropout/div_grad/Reshape*&
_output_shapes
:0

;gradients/Conv2/dropout/div_grad/tuple/control_dependency_1Identity*gradients/Conv2/dropout/div_grad/Reshape_12^gradients/Conv2/dropout/div_grad/tuple/group_deps*
_output_shapes
:*
T0*=
_class3
1/loc:@gradients/Conv2/dropout/div_grad/Reshape_1

(gradients/Conv1/BiasAdd_grad/BiasAddGradBiasAddGrad"gradients/Conv1/Relu_grad/ReluGrad*
_output_shapes
:*
T0*
data_formatNHWC

-gradients/Conv1/BiasAdd_grad/tuple/group_depsNoOp#^gradients/Conv1/Relu_grad/ReluGrad)^gradients/Conv1/BiasAdd_grad/BiasAddGrad

5gradients/Conv1/BiasAdd_grad/tuple/control_dependencyIdentity"gradients/Conv1/Relu_grad/ReluGrad.^gradients/Conv1/BiasAdd_grad/tuple/group_deps*
T0*5
_class+
)'loc:@gradients/Conv1/Relu_grad/ReluGrad*/
_output_shapes
:˙˙˙˙˙˙˙˙˙8~
˙
7gradients/Conv1/BiasAdd_grad/tuple/control_dependency_1Identity(gradients/Conv1/BiasAdd_grad/BiasAddGrad.^gradients/Conv1/BiasAdd_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients/Conv1/BiasAdd_grad/BiasAddGrad*
_output_shapes
:

gradients/AddN_3AddN2gradients/Cost/L2_regularization/L2Loss_1_grad/mul9gradients/Conv2/dropout/div_grad/tuple/control_dependency*
T0*E
_class;
97loc:@gradients/Cost/L2_regularization/L2Loss_1_grad/mul*
N*&
_output_shapes
:0
b
!gradients/Conv1/Conv2D_grad/ShapeShapeX*
T0*
out_type0*
_output_shapes
:
ŕ
/gradients/Conv1/Conv2D_grad/Conv2DBackpropInputConv2DBackpropInput!gradients/Conv1/Conv2D_grad/ShapeConv1/dropout/mul5gradients/Conv1/BiasAdd_grad/tuple/control_dependency*
T0*
strides
*
data_formatNHWC*
use_cudnn_on_gpu(*
paddingVALID*J
_output_shapes8
6:4˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
|
#gradients/Conv1/Conv2D_grad/Shape_1Const*%
valueB"            *
dtype0*
_output_shapes
:
°
0gradients/Conv1/Conv2D_grad/Conv2DBackpropFilterConv2DBackpropFilterX#gradients/Conv1/Conv2D_grad/Shape_15gradients/Conv1/BiasAdd_grad/tuple/control_dependency*
T0*
strides
*
data_formatNHWC*
use_cudnn_on_gpu(*
paddingVALID*&
_output_shapes
:

,gradients/Conv1/Conv2D_grad/tuple/group_depsNoOp0^gradients/Conv1/Conv2D_grad/Conv2DBackpropInput1^gradients/Conv1/Conv2D_grad/Conv2DBackpropFilter

4gradients/Conv1/Conv2D_grad/tuple/control_dependencyIdentity/gradients/Conv1/Conv2D_grad/Conv2DBackpropInput-^gradients/Conv1/Conv2D_grad/tuple/group_deps*
T0*B
_class8
64loc:@gradients/Conv1/Conv2D_grad/Conv2DBackpropInput*0
_output_shapes
:˙˙˙˙˙˙˙˙˙<

6gradients/Conv1/Conv2D_grad/tuple/control_dependency_1Identity0gradients/Conv1/Conv2D_grad/Conv2DBackpropFilter-^gradients/Conv1/Conv2D_grad/tuple/group_deps*
T0*C
_class9
75loc:@gradients/Conv1/Conv2D_grad/Conv2DBackpropFilter*&
_output_shapes
:

&gradients/Conv1/dropout/mul_grad/ShapeShapeConv1/dropout/div*
T0*
out_type0*#
_output_shapes
:˙˙˙˙˙˙˙˙˙

(gradients/Conv1/dropout/mul_grad/Shape_1ShapeConv1/dropout/Floor*
T0*
out_type0*#
_output_shapes
:˙˙˙˙˙˙˙˙˙
Ţ
6gradients/Conv1/dropout/mul_grad/BroadcastGradientArgsBroadcastGradientArgs&gradients/Conv1/dropout/mul_grad/Shape(gradients/Conv1/dropout/mul_grad/Shape_1*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙*
T0

$gradients/Conv1/dropout/mul_grad/mulMul6gradients/Conv1/Conv2D_grad/tuple/control_dependency_1Conv1/dropout/Floor*
_output_shapes
:*
T0
É
$gradients/Conv1/dropout/mul_grad/SumSum$gradients/Conv1/dropout/mul_grad/mul6gradients/Conv1/dropout/mul_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
˛
(gradients/Conv1/dropout/mul_grad/ReshapeReshape$gradients/Conv1/dropout/mul_grad/Sum&gradients/Conv1/dropout/mul_grad/Shape*
_output_shapes
:*
T0*
Tshape0

&gradients/Conv1/dropout/mul_grad/mul_1MulConv1/dropout/div6gradients/Conv1/Conv2D_grad/tuple/control_dependency_1*
T0*
_output_shapes
:
Ď
&gradients/Conv1/dropout/mul_grad/Sum_1Sum&gradients/Conv1/dropout/mul_grad/mul_18gradients/Conv1/dropout/mul_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
¸
*gradients/Conv1/dropout/mul_grad/Reshape_1Reshape&gradients/Conv1/dropout/mul_grad/Sum_1(gradients/Conv1/dropout/mul_grad/Shape_1*
T0*
Tshape0*
_output_shapes
:

1gradients/Conv1/dropout/mul_grad/tuple/group_depsNoOp)^gradients/Conv1/dropout/mul_grad/Reshape+^gradients/Conv1/dropout/mul_grad/Reshape_1

9gradients/Conv1/dropout/mul_grad/tuple/control_dependencyIdentity(gradients/Conv1/dropout/mul_grad/Reshape2^gradients/Conv1/dropout/mul_grad/tuple/group_deps*
_output_shapes
:*
T0*;
_class1
/-loc:@gradients/Conv1/dropout/mul_grad/Reshape

;gradients/Conv1/dropout/mul_grad/tuple/control_dependency_1Identity*gradients/Conv1/dropout/mul_grad/Reshape_12^gradients/Conv1/dropout/mul_grad/tuple/group_deps*=
_class3
1/loc:@gradients/Conv1/dropout/mul_grad/Reshape_1*
_output_shapes
:*
T0

&gradients/Conv1/dropout/div_grad/ShapeConst*
dtype0*
_output_shapes
:*%
valueB"            

(gradients/Conv1/dropout/div_grad/Shape_1Shapekeep_prob_conv/Placeholder*#
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0*
out_type0
Ţ
6gradients/Conv1/dropout/div_grad/BroadcastGradientArgsBroadcastGradientArgs&gradients/Conv1/dropout/div_grad/Shape(gradients/Conv1/dropout/div_grad/Shape_1*
T0*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙
­
(gradients/Conv1/dropout/div_grad/RealDivRealDiv9gradients/Conv1/dropout/mul_grad/tuple/control_dependencykeep_prob_conv/Placeholder*
T0*
_output_shapes
:
Í
$gradients/Conv1/dropout/div_grad/SumSum(gradients/Conv1/dropout/div_grad/RealDiv6gradients/Conv1/dropout/div_grad/BroadcastGradientArgs*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
Ŕ
(gradients/Conv1/dropout/div_grad/ReshapeReshape$gradients/Conv1/dropout/div_grad/Sum&gradients/Conv1/dropout/div_grad/Shape*
T0*
Tshape0*&
_output_shapes
:
j
$gradients/Conv1/dropout/div_grad/NegNegConv1/W/read*
T0*&
_output_shapes
:

*gradients/Conv1/dropout/div_grad/RealDiv_1RealDiv$gradients/Conv1/dropout/div_grad/Negkeep_prob_conv/Placeholder*
_output_shapes
:*
T0
 
*gradients/Conv1/dropout/div_grad/RealDiv_2RealDiv*gradients/Conv1/dropout/div_grad/RealDiv_1keep_prob_conv/Placeholder*
T0*
_output_shapes
:
ľ
$gradients/Conv1/dropout/div_grad/mulMul9gradients/Conv1/dropout/mul_grad/tuple/control_dependency*gradients/Conv1/dropout/div_grad/RealDiv_2*
_output_shapes
:*
T0
Í
&gradients/Conv1/dropout/div_grad/Sum_1Sum$gradients/Conv1/dropout/div_grad/mul8gradients/Conv1/dropout/div_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 
¸
*gradients/Conv1/dropout/div_grad/Reshape_1Reshape&gradients/Conv1/dropout/div_grad/Sum_1(gradients/Conv1/dropout/div_grad/Shape_1*
T0*
Tshape0*
_output_shapes
:

1gradients/Conv1/dropout/div_grad/tuple/group_depsNoOp)^gradients/Conv1/dropout/div_grad/Reshape+^gradients/Conv1/dropout/div_grad/Reshape_1

9gradients/Conv1/dropout/div_grad/tuple/control_dependencyIdentity(gradients/Conv1/dropout/div_grad/Reshape2^gradients/Conv1/dropout/div_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients/Conv1/dropout/div_grad/Reshape*&
_output_shapes
:

;gradients/Conv1/dropout/div_grad/tuple/control_dependency_1Identity*gradients/Conv1/dropout/div_grad/Reshape_12^gradients/Conv1/dropout/div_grad/tuple/group_deps*
T0*=
_class3
1/loc:@gradients/Conv1/dropout/div_grad/Reshape_1*
_output_shapes
:

gradients/AddN_4AddN0gradients/Cost/L2_regularization/L2Loss_grad/mul9gradients/Conv1/dropout/div_grad/tuple/control_dependency*
T0*C
_class9
75loc:@gradients/Cost/L2_regularization/L2Loss_grad/mul*
N*&
_output_shapes
:
z
beta1_power/initial_valueConst*
valueB
 *fff?*
_class
loc:@Conv1/W*
dtype0*
_output_shapes
: 

beta1_power
VariableV2*
shape: *
dtype0*
_output_shapes
: *
shared_name *
_class
loc:@Conv1/W*
	container 
Ş
beta1_power/AssignAssignbeta1_powerbeta1_power/initial_value*
_class
loc:@Conv1/W*
validate_shape(*
_output_shapes
: *
use_locking(*
T0
f
beta1_power/readIdentitybeta1_power*
_output_shapes
: *
T0*
_class
loc:@Conv1/W
z
beta2_power/initial_valueConst*
_output_shapes
: *
valueB
 *wž?*
_class
loc:@Conv1/W*
dtype0

beta2_power
VariableV2*
_class
loc:@Conv1/W*
	container *
shape: *
dtype0*
_output_shapes
: *
shared_name 
Ş
beta2_power/AssignAssignbeta2_powerbeta2_power/initial_value*
use_locking(*
T0*
_class
loc:@Conv1/W*
validate_shape(*
_output_shapes
: 
f
beta2_power/readIdentitybeta2_power*
T0*
_class
loc:@Conv1/W*
_output_shapes
: 

Conv1/W/Adam/Initializer/ConstConst*
_class
loc:@Conv1/W*%
valueB*    *
dtype0*&
_output_shapes
:
Ź
Conv1/W/Adam
VariableV2*
shape:*
dtype0*&
_output_shapes
:*
shared_name *
_class
loc:@Conv1/W*
	container 
Á
Conv1/W/Adam/AssignAssignConv1/W/AdamConv1/W/Adam/Initializer/Const*
use_locking(*
T0*
_class
loc:@Conv1/W*
validate_shape(*&
_output_shapes
:
x
Conv1/W/Adam/readIdentityConv1/W/Adam*&
_output_shapes
:*
T0*
_class
loc:@Conv1/W
Ą
 Conv1/W/Adam_1/Initializer/ConstConst*&
_output_shapes
:*
_class
loc:@Conv1/W*%
valueB*    *
dtype0
Ž
Conv1/W/Adam_1
VariableV2*
	container *
shape:*
dtype0*&
_output_shapes
:*
shared_name *
_class
loc:@Conv1/W
Ç
Conv1/W/Adam_1/AssignAssignConv1/W/Adam_1 Conv1/W/Adam_1/Initializer/Const*
validate_shape(*&
_output_shapes
:*
use_locking(*
T0*
_class
loc:@Conv1/W
|
Conv1/W/Adam_1/readIdentityConv1/W/Adam_1*
T0*
_class
loc:@Conv1/W*&
_output_shapes
:

Conv1/b/Adam/Initializer/ConstConst*
_class
loc:@Conv1/b*
valueB*    *
dtype0*
_output_shapes
:

Conv1/b/Adam
VariableV2*
	container *
shape:*
dtype0*
_output_shapes
:*
shared_name *
_class
loc:@Conv1/b
ľ
Conv1/b/Adam/AssignAssignConv1/b/AdamConv1/b/Adam/Initializer/Const*
use_locking(*
T0*
_class
loc:@Conv1/b*
validate_shape(*
_output_shapes
:
l
Conv1/b/Adam/readIdentityConv1/b/Adam*
T0*
_class
loc:@Conv1/b*
_output_shapes
:

 Conv1/b/Adam_1/Initializer/ConstConst*
dtype0*
_output_shapes
:*
_class
loc:@Conv1/b*
valueB*    

Conv1/b/Adam_1
VariableV2*
shared_name *
_class
loc:@Conv1/b*
	container *
shape:*
dtype0*
_output_shapes
:
ť
Conv1/b/Adam_1/AssignAssignConv1/b/Adam_1 Conv1/b/Adam_1/Initializer/Const*
use_locking(*
T0*
_class
loc:@Conv1/b*
validate_shape(*
_output_shapes
:
p
Conv1/b/Adam_1/readIdentityConv1/b/Adam_1*
T0*
_class
loc:@Conv1/b*
_output_shapes
:

Conv2/W/Adam/Initializer/ConstConst*
_class
loc:@Conv2/W*%
valueB0*    *
dtype0*&
_output_shapes
:0
Ź
Conv2/W/Adam
VariableV2*
shared_name *
_class
loc:@Conv2/W*
	container *
shape:0*
dtype0*&
_output_shapes
:0
Á
Conv2/W/Adam/AssignAssignConv2/W/AdamConv2/W/Adam/Initializer/Const*
use_locking(*
T0*
_class
loc:@Conv2/W*
validate_shape(*&
_output_shapes
:0
x
Conv2/W/Adam/readIdentityConv2/W/Adam*
T0*
_class
loc:@Conv2/W*&
_output_shapes
:0
Ą
 Conv2/W/Adam_1/Initializer/ConstConst*&
_output_shapes
:0*
_class
loc:@Conv2/W*%
valueB0*    *
dtype0
Ž
Conv2/W/Adam_1
VariableV2*&
_output_shapes
:0*
shared_name *
_class
loc:@Conv2/W*
	container *
shape:0*
dtype0
Ç
Conv2/W/Adam_1/AssignAssignConv2/W/Adam_1 Conv2/W/Adam_1/Initializer/Const*
use_locking(*
T0*
_class
loc:@Conv2/W*
validate_shape(*&
_output_shapes
:0
|
Conv2/W/Adam_1/readIdentityConv2/W/Adam_1*
T0*
_class
loc:@Conv2/W*&
_output_shapes
:0

Conv2/b/Adam/Initializer/ConstConst*
_class
loc:@Conv2/b*
valueB0*    *
dtype0*
_output_shapes
:0

Conv2/b/Adam
VariableV2*
_class
loc:@Conv2/b*
	container *
shape:0*
dtype0*
_output_shapes
:0*
shared_name 
ľ
Conv2/b/Adam/AssignAssignConv2/b/AdamConv2/b/Adam/Initializer/Const*
use_locking(*
T0*
_class
loc:@Conv2/b*
validate_shape(*
_output_shapes
:0
l
Conv2/b/Adam/readIdentityConv2/b/Adam*
_output_shapes
:0*
T0*
_class
loc:@Conv2/b

 Conv2/b/Adam_1/Initializer/ConstConst*
_class
loc:@Conv2/b*
valueB0*    *
dtype0*
_output_shapes
:0

Conv2/b/Adam_1
VariableV2*
shared_name *
_class
loc:@Conv2/b*
	container *
shape:0*
dtype0*
_output_shapes
:0
ť
Conv2/b/Adam_1/AssignAssignConv2/b/Adam_1 Conv2/b/Adam_1/Initializer/Const*
T0*
_class
loc:@Conv2/b*
validate_shape(*
_output_shapes
:0*
use_locking(
p
Conv2/b/Adam_1/readIdentityConv2/b/Adam_1*
T0*
_class
loc:@Conv2/b*
_output_shapes
:0

Conv3/W/Adam/Initializer/ConstConst*
_class
loc:@Conv3/W*%
valueB00*    *
dtype0*&
_output_shapes
:00
Ź
Conv3/W/Adam
VariableV2*
dtype0*&
_output_shapes
:00*
shared_name *
_class
loc:@Conv3/W*
	container *
shape:00
Á
Conv3/W/Adam/AssignAssignConv3/W/AdamConv3/W/Adam/Initializer/Const*
validate_shape(*&
_output_shapes
:00*
use_locking(*
T0*
_class
loc:@Conv3/W
x
Conv3/W/Adam/readIdentityConv3/W/Adam*&
_output_shapes
:00*
T0*
_class
loc:@Conv3/W
Ą
 Conv3/W/Adam_1/Initializer/ConstConst*
_class
loc:@Conv3/W*%
valueB00*    *
dtype0*&
_output_shapes
:00
Ž
Conv3/W/Adam_1
VariableV2*
_class
loc:@Conv3/W*
	container *
shape:00*
dtype0*&
_output_shapes
:00*
shared_name 
Ç
Conv3/W/Adam_1/AssignAssignConv3/W/Adam_1 Conv3/W/Adam_1/Initializer/Const*
T0*
_class
loc:@Conv3/W*
validate_shape(*&
_output_shapes
:00*
use_locking(
|
Conv3/W/Adam_1/readIdentityConv3/W/Adam_1*
T0*
_class
loc:@Conv3/W*&
_output_shapes
:00

Conv3/b/Adam/Initializer/ConstConst*
_class
loc:@Conv3/b*
valueB0*    *
dtype0*
_output_shapes
:0

Conv3/b/Adam
VariableV2*
shared_name *
_class
loc:@Conv3/b*
	container *
shape:0*
dtype0*
_output_shapes
:0
ľ
Conv3/b/Adam/AssignAssignConv3/b/AdamConv3/b/Adam/Initializer/Const*
T0*
_class
loc:@Conv3/b*
validate_shape(*
_output_shapes
:0*
use_locking(
l
Conv3/b/Adam/readIdentityConv3/b/Adam*
_output_shapes
:0*
T0*
_class
loc:@Conv3/b

 Conv3/b/Adam_1/Initializer/ConstConst*
dtype0*
_output_shapes
:0*
_class
loc:@Conv3/b*
valueB0*    

Conv3/b/Adam_1
VariableV2*
dtype0*
_output_shapes
:0*
shared_name *
_class
loc:@Conv3/b*
	container *
shape:0
ť
Conv3/b/Adam_1/AssignAssignConv3/b/Adam_1 Conv3/b/Adam_1/Initializer/Const*
_class
loc:@Conv3/b*
validate_shape(*
_output_shapes
:0*
use_locking(*
T0
p
Conv3/b/Adam_1/readIdentityConv3/b/Adam_1*
T0*
_class
loc:@Conv3/b*
_output_shapes
:0

FC1/W/Adam/Initializer/ConstConst*
_class

loc:@FC1/W*
valueB	@*    *
dtype0*
_output_shapes
:	@


FC1/W/Adam
VariableV2*
	container *
shape:	@*
dtype0*
_output_shapes
:	@*
shared_name *
_class

loc:@FC1/W
˛
FC1/W/Adam/AssignAssign
FC1/W/AdamFC1/W/Adam/Initializer/Const*
_output_shapes
:	@*
use_locking(*
T0*
_class

loc:@FC1/W*
validate_shape(
k
FC1/W/Adam/readIdentity
FC1/W/Adam*
_output_shapes
:	@*
T0*
_class

loc:@FC1/W

FC1/W/Adam_1/Initializer/ConstConst*
_class

loc:@FC1/W*
valueB	@*    *
dtype0*
_output_shapes
:	@

FC1/W/Adam_1
VariableV2*
	container *
shape:	@*
dtype0*
_output_shapes
:	@*
shared_name *
_class

loc:@FC1/W
¸
FC1/W/Adam_1/AssignAssignFC1/W/Adam_1FC1/W/Adam_1/Initializer/Const*
_class

loc:@FC1/W*
validate_shape(*
_output_shapes
:	@*
use_locking(*
T0
o
FC1/W/Adam_1/readIdentityFC1/W/Adam_1*
_class

loc:@FC1/W*
_output_shapes
:	@*
T0

FC1/b/Adam/Initializer/ConstConst*
dtype0*
_output_shapes
:@*
_class

loc:@FC1/b*
valueB@*    


FC1/b/Adam
VariableV2*
shape:@*
dtype0*
_output_shapes
:@*
shared_name *
_class

loc:@FC1/b*
	container 
­
FC1/b/Adam/AssignAssign
FC1/b/AdamFC1/b/Adam/Initializer/Const*
use_locking(*
T0*
_class

loc:@FC1/b*
validate_shape(*
_output_shapes
:@
f
FC1/b/Adam/readIdentity
FC1/b/Adam*
T0*
_class

loc:@FC1/b*
_output_shapes
:@

FC1/b/Adam_1/Initializer/ConstConst*
_class

loc:@FC1/b*
valueB@*    *
dtype0*
_output_shapes
:@

FC1/b/Adam_1
VariableV2*
dtype0*
_output_shapes
:@*
shared_name *
_class

loc:@FC1/b*
	container *
shape:@
ł
FC1/b/Adam_1/AssignAssignFC1/b/Adam_1FC1/b/Adam_1/Initializer/Const*
use_locking(*
T0*
_class

loc:@FC1/b*
validate_shape(*
_output_shapes
:@
j
FC1/b/Adam_1/readIdentityFC1/b/Adam_1*
T0*
_class

loc:@FC1/b*
_output_shapes
:@

FC2/W/Adam/Initializer/ConstConst*
_class

loc:@FC2/W*
valueB@
*    *
dtype0*
_output_shapes

:@



FC2/W/Adam
VariableV2*
shared_name *
_class

loc:@FC2/W*
	container *
shape
:@
*
dtype0*
_output_shapes

:@

ą
FC2/W/Adam/AssignAssign
FC2/W/AdamFC2/W/Adam/Initializer/Const*
use_locking(*
T0*
_class

loc:@FC2/W*
validate_shape(*
_output_shapes

:@

j
FC2/W/Adam/readIdentity
FC2/W/Adam*
T0*
_class

loc:@FC2/W*
_output_shapes

:@


FC2/W/Adam_1/Initializer/ConstConst*
_class

loc:@FC2/W*
valueB@
*    *
dtype0*
_output_shapes

:@


FC2/W/Adam_1
VariableV2*
shared_name *
_class

loc:@FC2/W*
	container *
shape
:@
*
dtype0*
_output_shapes

:@

ˇ
FC2/W/Adam_1/AssignAssignFC2/W/Adam_1FC2/W/Adam_1/Initializer/Const*
use_locking(*
T0*
_class

loc:@FC2/W*
validate_shape(*
_output_shapes

:@

n
FC2/W/Adam_1/readIdentityFC2/W/Adam_1*
T0*
_class

loc:@FC2/W*
_output_shapes

:@


FC2/b/Adam/Initializer/ConstConst*
_class

loc:@FC2/b*
valueB
*    *
dtype0*
_output_shapes
:



FC2/b/Adam
VariableV2*
_output_shapes
:
*
shared_name *
_class

loc:@FC2/b*
	container *
shape:
*
dtype0
­
FC2/b/Adam/AssignAssign
FC2/b/AdamFC2/b/Adam/Initializer/Const*
use_locking(*
T0*
_class

loc:@FC2/b*
validate_shape(*
_output_shapes
:

f
FC2/b/Adam/readIdentity
FC2/b/Adam*
_output_shapes
:
*
T0*
_class

loc:@FC2/b

FC2/b/Adam_1/Initializer/ConstConst*
_class

loc:@FC2/b*
valueB
*    *
dtype0*
_output_shapes
:


FC2/b/Adam_1
VariableV2*
	container *
shape:
*
dtype0*
_output_shapes
:
*
shared_name *
_class

loc:@FC2/b
ł
FC2/b/Adam_1/AssignAssignFC2/b/Adam_1FC2/b/Adam_1/Initializer/Const*
T0*
_class

loc:@FC2/b*
validate_shape(*
_output_shapes
:
*
use_locking(
j
FC2/b/Adam_1/readIdentityFC2/b/Adam_1*
T0*
_class

loc:@FC2/b*
_output_shapes
:

W
Adam/learning_rateConst*
valueB
 *o:*
dtype0*
_output_shapes
: 
O

Adam/beta1Const*
_output_shapes
: *
valueB
 *fff?*
dtype0
O

Adam/beta2Const*
valueB
 *wž?*
dtype0*
_output_shapes
: 
Q
Adam/epsilonConst*
valueB
 *wĚ+2*
dtype0*
_output_shapes
: 
Ą
Adam/update_Conv1/W/ApplyAdam	ApplyAdamConv1/WConv1/W/AdamConv1/W/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilongradients/AddN_4*&
_output_shapes
:*
use_locking( *
T0*
_class
loc:@Conv1/W
ź
Adam/update_Conv1/b/ApplyAdam	ApplyAdamConv1/bConv1/b/AdamConv1/b/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon7gradients/Conv1/BiasAdd_grad/tuple/control_dependency_1*
_output_shapes
:*
use_locking( *
T0*
_class
loc:@Conv1/b
Ą
Adam/update_Conv2/W/ApplyAdam	ApplyAdamConv2/WConv2/W/AdamConv2/W/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilongradients/AddN_3*
use_locking( *
T0*
_class
loc:@Conv2/W*&
_output_shapes
:0
ź
Adam/update_Conv2/b/ApplyAdam	ApplyAdamConv2/bConv2/b/AdamConv2/b/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon7gradients/Conv2/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0*
_class
loc:@Conv2/b*
_output_shapes
:0
Ą
Adam/update_Conv3/W/ApplyAdam	ApplyAdamConv3/WConv3/W/AdamConv3/W/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilongradients/AddN_2*
use_locking( *
T0*
_class
loc:@Conv3/W*&
_output_shapes
:00
ź
Adam/update_Conv3/b/ApplyAdam	ApplyAdamConv3/bConv3/b/AdamConv3/b/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon7gradients/Conv3/BiasAdd_grad/tuple/control_dependency_1*
_output_shapes
:0*
use_locking( *
T0*
_class
loc:@Conv3/b

Adam/update_FC1/W/ApplyAdam	ApplyAdamFC1/W
FC1/W/AdamFC1/W/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilongradients/AddN_1*
use_locking( *
T0*
_class

loc:@FC1/W*
_output_shapes
:	@
Ź
Adam/update_FC1/b/ApplyAdam	ApplyAdamFC1/b
FC1/b/AdamFC1/b/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon1gradients/FC1/add_grad/tuple/control_dependency_1*
_output_shapes
:@*
use_locking( *
T0*
_class

loc:@FC1/b

Adam/update_FC2/W/ApplyAdam	ApplyAdamFC2/W
FC2/W/AdamFC2/W/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilongradients/AddN*
_output_shapes

:@
*
use_locking( *
T0*
_class

loc:@FC2/W
Ź
Adam/update_FC2/b/ApplyAdam	ApplyAdamFC2/b
FC2/b/AdamFC2/b/Adam_1beta1_power/readbeta2_power/readAdam/learning_rate
Adam/beta1
Adam/beta2Adam/epsilon1gradients/FC2/add_grad/tuple/control_dependency_1*
_class

loc:@FC2/b*
_output_shapes
:
*
use_locking( *
T0
˘
Adam/mulMulbeta1_power/read
Adam/beta1^Adam/update_Conv1/W/ApplyAdam^Adam/update_Conv1/b/ApplyAdam^Adam/update_Conv2/W/ApplyAdam^Adam/update_Conv2/b/ApplyAdam^Adam/update_Conv3/W/ApplyAdam^Adam/update_Conv3/b/ApplyAdam^Adam/update_FC1/W/ApplyAdam^Adam/update_FC1/b/ApplyAdam^Adam/update_FC2/W/ApplyAdam^Adam/update_FC2/b/ApplyAdam*
T0*
_class
loc:@Conv1/W*
_output_shapes
: 

Adam/AssignAssignbeta1_powerAdam/mul*
validate_shape(*
_output_shapes
: *
use_locking( *
T0*
_class
loc:@Conv1/W
¤

Adam/mul_1Mulbeta2_power/read
Adam/beta2^Adam/update_Conv1/W/ApplyAdam^Adam/update_Conv1/b/ApplyAdam^Adam/update_Conv2/W/ApplyAdam^Adam/update_Conv2/b/ApplyAdam^Adam/update_Conv3/W/ApplyAdam^Adam/update_Conv3/b/ApplyAdam^Adam/update_FC1/W/ApplyAdam^Adam/update_FC1/b/ApplyAdam^Adam/update_FC2/W/ApplyAdam^Adam/update_FC2/b/ApplyAdam*
_class
loc:@Conv1/W*
_output_shapes
: *
T0

Adam/Assign_1Assignbeta2_power
Adam/mul_1*
use_locking( *
T0*
_class
loc:@Conv1/W*
validate_shape(*
_output_shapes
: 
â
AdamNoOp^Adam/update_Conv1/W/ApplyAdam^Adam/update_Conv1/b/ApplyAdam^Adam/update_Conv2/W/ApplyAdam^Adam/update_Conv2/b/ApplyAdam^Adam/update_Conv3/W/ApplyAdam^Adam/update_Conv3/b/ApplyAdam^Adam/update_FC1/W/ApplyAdam^Adam/update_FC1/b/ApplyAdam^Adam/update_FC2/W/ApplyAdam^Adam/update_FC2/b/ApplyAdam^Adam/Assign^Adam/Assign_1

initNoOp^Conv1/W/Assign^Conv1/b/Assign^Conv2/W/Assign^Conv2/b/Assign^Conv3/W/Assign^Conv3/b/Assign^FC1/W/Assign^FC1/b/Assign^FC2/W/Assign^FC2/b/Assign^beta1_power/Assign^beta2_power/Assign^Conv1/W/Adam/Assign^Conv1/W/Adam_1/Assign^Conv1/b/Adam/Assign^Conv1/b/Adam_1/Assign^Conv2/W/Adam/Assign^Conv2/W/Adam_1/Assign^Conv2/b/Adam/Assign^Conv2/b/Adam_1/Assign^Conv3/W/Adam/Assign^Conv3/W/Adam_1/Assign^Conv3/b/Adam/Assign^Conv3/b/Adam_1/Assign^FC1/W/Adam/Assign^FC1/W/Adam_1/Assign^FC1/b/Adam/Assign^FC1/b/Adam_1/Assign^FC2/W/Adam/Assign^FC2/W/Adam_1/Assign^FC2/b/Adam/Assign^FC2/b/Adam_1/Assign
[
accuracy/ArgMax/dimensionConst*
value	B :*
dtype0*
_output_shapes
: 
w
accuracy/ArgMaxArgMaxFC2/addaccuracy/ArgMax/dimension*

Tidx0*
T0*#
_output_shapes
:˙˙˙˙˙˙˙˙˙
]
accuracy/ArgMax_1/dimensionConst*
_output_shapes
: *
value	B :*
dtype0
u
accuracy/ArgMax_1ArgMaxYaccuracy/ArgMax_1/dimension*#
_output_shapes
:˙˙˙˙˙˙˙˙˙*

Tidx0*
T0
i
accuracy/EqualEqualaccuracy/ArgMaxaccuracy/ArgMax_1*
T0	*#
_output_shapes
:˙˙˙˙˙˙˙˙˙
b
accuracy/CastCastaccuracy/Equal*

SrcT0
*#
_output_shapes
:˙˙˙˙˙˙˙˙˙*

DstT0
X
accuracy/ConstConst*
valueB: *
dtype0*
_output_shapes
:
r
accuracy/MeanMeanaccuracy/Castaccuracy/Const*

Tidx0*
	keep_dims( *
T0*
_output_shapes
: 

%accuracy/per_epoch_per_minibatch/tagsConst*
dtype0*
_output_shapes
: *1
value(B& B accuracy/per_epoch_per_minibatch

 accuracy/per_epoch_per_minibatchScalarSummary%accuracy/per_epoch_per_minibatch/tagsaccuracy/Mean*
T0*
_output_shapes
: 
Ú
Merge/MergeSummaryMergeSummaryConv1/weightsConv1/biasesConv2/weightsConv2/biasesConv3/weightsConv3/biasesCost/per_epoch_per_minibatch accuracy/per_epoch_per_minibatch*
N*
_output_shapes
: 
P

save/ConstConst*
valueB Bmodel*
dtype0*
_output_shapes
: 

save/StringJoin/inputs_1Const*<
value3B1 B+_temp_db4e0e0ff2be4f1a80376bb72a283e6b/part*
dtype0*
_output_shapes
: 
u
save/StringJoin
StringJoin
save/Constsave/StringJoin/inputs_1*
N*
_output_shapes
: *
	separator 
Q
save/num_shardsConst*
value	B :*
dtype0*
_output_shapes
: 
\
save/ShardedFilename/shardConst*
value	B : *
dtype0*
_output_shapes
: 
}
save/ShardedFilenameShardedFilenamesave/StringJoinsave/ShardedFilename/shardsave/num_shards*
_output_shapes
: 
ę
save/SaveV2/tensor_namesConst*
valueB BConv1/WBConv1/W/AdamBConv1/W/Adam_1BConv1/bBConv1/b/AdamBConv1/b/Adam_1BConv2/WBConv2/W/AdamBConv2/W/Adam_1BConv2/bBConv2/b/AdamBConv2/b/Adam_1BConv3/WBConv3/W/AdamBConv3/W/Adam_1BConv3/bBConv3/b/AdamBConv3/b/Adam_1BFC1/WB
FC1/W/AdamBFC1/W/Adam_1BFC1/bB
FC1/b/AdamBFC1/b/Adam_1BFC2/WB
FC2/W/AdamBFC2/W/Adam_1BFC2/bB
FC2/b/AdamBFC2/b/Adam_1Bbeta1_powerBbeta2_power*
dtype0*
_output_shapes
: 
Ł
save/SaveV2/shape_and_slicesConst*
dtype0*
_output_shapes
: *S
valueJBH B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 

save/SaveV2SaveV2save/ShardedFilenamesave/SaveV2/tensor_namessave/SaveV2/shape_and_slicesConv1/WConv1/W/AdamConv1/W/Adam_1Conv1/bConv1/b/AdamConv1/b/Adam_1Conv2/WConv2/W/AdamConv2/W/Adam_1Conv2/bConv2/b/AdamConv2/b/Adam_1Conv3/WConv3/W/AdamConv3/W/Adam_1Conv3/bConv3/b/AdamConv3/b/Adam_1FC1/W
FC1/W/AdamFC1/W/Adam_1FC1/b
FC1/b/AdamFC1/b/Adam_1FC2/W
FC2/W/AdamFC2/W/Adam_1FC2/b
FC2/b/AdamFC2/b/Adam_1beta1_powerbeta2_power*.
dtypes$
"2 

save/control_dependencyIdentitysave/ShardedFilename^save/SaveV2*
_output_shapes
: *
T0*'
_class
loc:@save/ShardedFilename

+save/MergeV2Checkpoints/checkpoint_prefixesPacksave/ShardedFilename^save/control_dependency*
T0*

axis *
N*
_output_shapes
:
}
save/MergeV2CheckpointsMergeV2Checkpoints+save/MergeV2Checkpoints/checkpoint_prefixes
save/Const*
delete_old_dirs(
z
save/IdentityIdentity
save/Const^save/control_dependency^save/MergeV2Checkpoints*
_output_shapes
: *
T0
k
save/RestoreV2/tensor_namesConst*
valueBBConv1/W*
dtype0*
_output_shapes
:
h
save/RestoreV2/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2	RestoreV2
save/Constsave/RestoreV2/tensor_namessave/RestoreV2/shape_and_slices*
_output_shapes
:*
dtypes
2
¤
save/AssignAssignConv1/Wsave/RestoreV2*
use_locking(*
T0*
_class
loc:@Conv1/W*
validate_shape(*&
_output_shapes
:
r
save/RestoreV2_1/tensor_namesConst*!
valueBBConv1/W/Adam*
dtype0*
_output_shapes
:
j
!save/RestoreV2_1/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_1	RestoreV2
save/Constsave/RestoreV2_1/tensor_names!save/RestoreV2_1/shape_and_slices*
_output_shapes
:*
dtypes
2
­
save/Assign_1AssignConv1/W/Adamsave/RestoreV2_1*
T0*
_class
loc:@Conv1/W*
validate_shape(*&
_output_shapes
:*
use_locking(
t
save/RestoreV2_2/tensor_namesConst*#
valueBBConv1/W/Adam_1*
dtype0*
_output_shapes
:
j
!save/RestoreV2_2/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_2	RestoreV2
save/Constsave/RestoreV2_2/tensor_names!save/RestoreV2_2/shape_and_slices*
_output_shapes
:*
dtypes
2
Ż
save/Assign_2AssignConv1/W/Adam_1save/RestoreV2_2*&
_output_shapes
:*
use_locking(*
T0*
_class
loc:@Conv1/W*
validate_shape(
m
save/RestoreV2_3/tensor_namesConst*
valueBBConv1/b*
dtype0*
_output_shapes
:
j
!save/RestoreV2_3/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_3	RestoreV2
save/Constsave/RestoreV2_3/tensor_names!save/RestoreV2_3/shape_and_slices*
_output_shapes
:*
dtypes
2

save/Assign_3AssignConv1/bsave/RestoreV2_3*
T0*
_class
loc:@Conv1/b*
validate_shape(*
_output_shapes
:*
use_locking(
r
save/RestoreV2_4/tensor_namesConst*!
valueBBConv1/b/Adam*
dtype0*
_output_shapes
:
j
!save/RestoreV2_4/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_4	RestoreV2
save/Constsave/RestoreV2_4/tensor_names!save/RestoreV2_4/shape_and_slices*
_output_shapes
:*
dtypes
2
Ą
save/Assign_4AssignConv1/b/Adamsave/RestoreV2_4*
use_locking(*
T0*
_class
loc:@Conv1/b*
validate_shape(*
_output_shapes
:
t
save/RestoreV2_5/tensor_namesConst*#
valueBBConv1/b/Adam_1*
dtype0*
_output_shapes
:
j
!save/RestoreV2_5/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_5	RestoreV2
save/Constsave/RestoreV2_5/tensor_names!save/RestoreV2_5/shape_and_slices*
dtypes
2*
_output_shapes
:
Ł
save/Assign_5AssignConv1/b/Adam_1save/RestoreV2_5*
use_locking(*
T0*
_class
loc:@Conv1/b*
validate_shape(*
_output_shapes
:
m
save/RestoreV2_6/tensor_namesConst*
valueBBConv2/W*
dtype0*
_output_shapes
:
j
!save/RestoreV2_6/shape_and_slicesConst*
dtype0*
_output_shapes
:*
valueB
B 

save/RestoreV2_6	RestoreV2
save/Constsave/RestoreV2_6/tensor_names!save/RestoreV2_6/shape_and_slices*
_output_shapes
:*
dtypes
2
¨
save/Assign_6AssignConv2/Wsave/RestoreV2_6*
use_locking(*
T0*
_class
loc:@Conv2/W*
validate_shape(*&
_output_shapes
:0
r
save/RestoreV2_7/tensor_namesConst*
_output_shapes
:*!
valueBBConv2/W/Adam*
dtype0
j
!save/RestoreV2_7/shape_and_slicesConst*
dtype0*
_output_shapes
:*
valueB
B 

save/RestoreV2_7	RestoreV2
save/Constsave/RestoreV2_7/tensor_names!save/RestoreV2_7/shape_and_slices*
dtypes
2*
_output_shapes
:
­
save/Assign_7AssignConv2/W/Adamsave/RestoreV2_7*
use_locking(*
T0*
_class
loc:@Conv2/W*
validate_shape(*&
_output_shapes
:0
t
save/RestoreV2_8/tensor_namesConst*#
valueBBConv2/W/Adam_1*
dtype0*
_output_shapes
:
j
!save/RestoreV2_8/shape_and_slicesConst*
_output_shapes
:*
valueB
B *
dtype0

save/RestoreV2_8	RestoreV2
save/Constsave/RestoreV2_8/tensor_names!save/RestoreV2_8/shape_and_slices*
dtypes
2*
_output_shapes
:
Ż
save/Assign_8AssignConv2/W/Adam_1save/RestoreV2_8*
validate_shape(*&
_output_shapes
:0*
use_locking(*
T0*
_class
loc:@Conv2/W
m
save/RestoreV2_9/tensor_namesConst*
valueBBConv2/b*
dtype0*
_output_shapes
:
j
!save/RestoreV2_9/shape_and_slicesConst*
dtype0*
_output_shapes
:*
valueB
B 

save/RestoreV2_9	RestoreV2
save/Constsave/RestoreV2_9/tensor_names!save/RestoreV2_9/shape_and_slices*
_output_shapes
:*
dtypes
2

save/Assign_9AssignConv2/bsave/RestoreV2_9*
use_locking(*
T0*
_class
loc:@Conv2/b*
validate_shape(*
_output_shapes
:0
s
save/RestoreV2_10/tensor_namesConst*!
valueBBConv2/b/Adam*
dtype0*
_output_shapes
:
k
"save/RestoreV2_10/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_10	RestoreV2
save/Constsave/RestoreV2_10/tensor_names"save/RestoreV2_10/shape_and_slices*
_output_shapes
:*
dtypes
2
Ł
save/Assign_10AssignConv2/b/Adamsave/RestoreV2_10*
_output_shapes
:0*
use_locking(*
T0*
_class
loc:@Conv2/b*
validate_shape(
u
save/RestoreV2_11/tensor_namesConst*
_output_shapes
:*#
valueBBConv2/b/Adam_1*
dtype0
k
"save/RestoreV2_11/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_11	RestoreV2
save/Constsave/RestoreV2_11/tensor_names"save/RestoreV2_11/shape_and_slices*
_output_shapes
:*
dtypes
2
Ľ
save/Assign_11AssignConv2/b/Adam_1save/RestoreV2_11*
_output_shapes
:0*
use_locking(*
T0*
_class
loc:@Conv2/b*
validate_shape(
n
save/RestoreV2_12/tensor_namesConst*
valueBBConv3/W*
dtype0*
_output_shapes
:
k
"save/RestoreV2_12/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_12	RestoreV2
save/Constsave/RestoreV2_12/tensor_names"save/RestoreV2_12/shape_and_slices*
_output_shapes
:*
dtypes
2
Ş
save/Assign_12AssignConv3/Wsave/RestoreV2_12*
T0*
_class
loc:@Conv3/W*
validate_shape(*&
_output_shapes
:00*
use_locking(
s
save/RestoreV2_13/tensor_namesConst*!
valueBBConv3/W/Adam*
dtype0*
_output_shapes
:
k
"save/RestoreV2_13/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_13	RestoreV2
save/Constsave/RestoreV2_13/tensor_names"save/RestoreV2_13/shape_and_slices*
_output_shapes
:*
dtypes
2
Ż
save/Assign_13AssignConv3/W/Adamsave/RestoreV2_13*
validate_shape(*&
_output_shapes
:00*
use_locking(*
T0*
_class
loc:@Conv3/W
u
save/RestoreV2_14/tensor_namesConst*#
valueBBConv3/W/Adam_1*
dtype0*
_output_shapes
:
k
"save/RestoreV2_14/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_14	RestoreV2
save/Constsave/RestoreV2_14/tensor_names"save/RestoreV2_14/shape_and_slices*
_output_shapes
:*
dtypes
2
ą
save/Assign_14AssignConv3/W/Adam_1save/RestoreV2_14*
use_locking(*
T0*
_class
loc:@Conv3/W*
validate_shape(*&
_output_shapes
:00
n
save/RestoreV2_15/tensor_namesConst*
dtype0*
_output_shapes
:*
valueBBConv3/b
k
"save/RestoreV2_15/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_15	RestoreV2
save/Constsave/RestoreV2_15/tensor_names"save/RestoreV2_15/shape_and_slices*
_output_shapes
:*
dtypes
2

save/Assign_15AssignConv3/bsave/RestoreV2_15*
validate_shape(*
_output_shapes
:0*
use_locking(*
T0*
_class
loc:@Conv3/b
s
save/RestoreV2_16/tensor_namesConst*!
valueBBConv3/b/Adam*
dtype0*
_output_shapes
:
k
"save/RestoreV2_16/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_16	RestoreV2
save/Constsave/RestoreV2_16/tensor_names"save/RestoreV2_16/shape_and_slices*
_output_shapes
:*
dtypes
2
Ł
save/Assign_16AssignConv3/b/Adamsave/RestoreV2_16*
T0*
_class
loc:@Conv3/b*
validate_shape(*
_output_shapes
:0*
use_locking(
u
save/RestoreV2_17/tensor_namesConst*
_output_shapes
:*#
valueBBConv3/b/Adam_1*
dtype0
k
"save/RestoreV2_17/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_17	RestoreV2
save/Constsave/RestoreV2_17/tensor_names"save/RestoreV2_17/shape_and_slices*
_output_shapes
:*
dtypes
2
Ľ
save/Assign_17AssignConv3/b/Adam_1save/RestoreV2_17*
use_locking(*
T0*
_class
loc:@Conv3/b*
validate_shape(*
_output_shapes
:0
l
save/RestoreV2_18/tensor_namesConst*
valueBBFC1/W*
dtype0*
_output_shapes
:
k
"save/RestoreV2_18/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_18	RestoreV2
save/Constsave/RestoreV2_18/tensor_names"save/RestoreV2_18/shape_and_slices*
dtypes
2*
_output_shapes
:

save/Assign_18AssignFC1/Wsave/RestoreV2_18*
_class

loc:@FC1/W*
validate_shape(*
_output_shapes
:	@*
use_locking(*
T0
q
save/RestoreV2_19/tensor_namesConst*
valueBB
FC1/W/Adam*
dtype0*
_output_shapes
:
k
"save/RestoreV2_19/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_19	RestoreV2
save/Constsave/RestoreV2_19/tensor_names"save/RestoreV2_19/shape_and_slices*
_output_shapes
:*
dtypes
2
¤
save/Assign_19Assign
FC1/W/Adamsave/RestoreV2_19*
_class

loc:@FC1/W*
validate_shape(*
_output_shapes
:	@*
use_locking(*
T0
s
save/RestoreV2_20/tensor_namesConst*
_output_shapes
:*!
valueBBFC1/W/Adam_1*
dtype0
k
"save/RestoreV2_20/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_20	RestoreV2
save/Constsave/RestoreV2_20/tensor_names"save/RestoreV2_20/shape_and_slices*
_output_shapes
:*
dtypes
2
Ś
save/Assign_20AssignFC1/W/Adam_1save/RestoreV2_20*
use_locking(*
T0*
_class

loc:@FC1/W*
validate_shape(*
_output_shapes
:	@
l
save/RestoreV2_21/tensor_namesConst*
dtype0*
_output_shapes
:*
valueBBFC1/b
k
"save/RestoreV2_21/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_21	RestoreV2
save/Constsave/RestoreV2_21/tensor_names"save/RestoreV2_21/shape_and_slices*
_output_shapes
:*
dtypes
2

save/Assign_21AssignFC1/bsave/RestoreV2_21*
use_locking(*
T0*
_class

loc:@FC1/b*
validate_shape(*
_output_shapes
:@
q
save/RestoreV2_22/tensor_namesConst*
valueBB
FC1/b/Adam*
dtype0*
_output_shapes
:
k
"save/RestoreV2_22/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_22	RestoreV2
save/Constsave/RestoreV2_22/tensor_names"save/RestoreV2_22/shape_and_slices*
_output_shapes
:*
dtypes
2

save/Assign_22Assign
FC1/b/Adamsave/RestoreV2_22*
use_locking(*
T0*
_class

loc:@FC1/b*
validate_shape(*
_output_shapes
:@
s
save/RestoreV2_23/tensor_namesConst*!
valueBBFC1/b/Adam_1*
dtype0*
_output_shapes
:
k
"save/RestoreV2_23/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_23	RestoreV2
save/Constsave/RestoreV2_23/tensor_names"save/RestoreV2_23/shape_and_slices*
dtypes
2*
_output_shapes
:
Ą
save/Assign_23AssignFC1/b/Adam_1save/RestoreV2_23*
use_locking(*
T0*
_class

loc:@FC1/b*
validate_shape(*
_output_shapes
:@
l
save/RestoreV2_24/tensor_namesConst*
valueBBFC2/W*
dtype0*
_output_shapes
:
k
"save/RestoreV2_24/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_24	RestoreV2
save/Constsave/RestoreV2_24/tensor_names"save/RestoreV2_24/shape_and_slices*
_output_shapes
:*
dtypes
2

save/Assign_24AssignFC2/Wsave/RestoreV2_24*
use_locking(*
T0*
_class

loc:@FC2/W*
validate_shape(*
_output_shapes

:@

q
save/RestoreV2_25/tensor_namesConst*
valueBB
FC2/W/Adam*
dtype0*
_output_shapes
:
k
"save/RestoreV2_25/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_25	RestoreV2
save/Constsave/RestoreV2_25/tensor_names"save/RestoreV2_25/shape_and_slices*
_output_shapes
:*
dtypes
2
Ł
save/Assign_25Assign
FC2/W/Adamsave/RestoreV2_25*
use_locking(*
T0*
_class

loc:@FC2/W*
validate_shape(*
_output_shapes

:@

s
save/RestoreV2_26/tensor_namesConst*!
valueBBFC2/W/Adam_1*
dtype0*
_output_shapes
:
k
"save/RestoreV2_26/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_26	RestoreV2
save/Constsave/RestoreV2_26/tensor_names"save/RestoreV2_26/shape_and_slices*
_output_shapes
:*
dtypes
2
Ľ
save/Assign_26AssignFC2/W/Adam_1save/RestoreV2_26*
validate_shape(*
_output_shapes

:@
*
use_locking(*
T0*
_class

loc:@FC2/W
l
save/RestoreV2_27/tensor_namesConst*
_output_shapes
:*
valueBBFC2/b*
dtype0
k
"save/RestoreV2_27/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_27	RestoreV2
save/Constsave/RestoreV2_27/tensor_names"save/RestoreV2_27/shape_and_slices*
dtypes
2*
_output_shapes
:

save/Assign_27AssignFC2/bsave/RestoreV2_27*
validate_shape(*
_output_shapes
:
*
use_locking(*
T0*
_class

loc:@FC2/b
q
save/RestoreV2_28/tensor_namesConst*
dtype0*
_output_shapes
:*
valueBB
FC2/b/Adam
k
"save/RestoreV2_28/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_28	RestoreV2
save/Constsave/RestoreV2_28/tensor_names"save/RestoreV2_28/shape_and_slices*
_output_shapes
:*
dtypes
2

save/Assign_28Assign
FC2/b/Adamsave/RestoreV2_28*
use_locking(*
T0*
_class

loc:@FC2/b*
validate_shape(*
_output_shapes
:

s
save/RestoreV2_29/tensor_namesConst*
_output_shapes
:*!
valueBBFC2/b/Adam_1*
dtype0
k
"save/RestoreV2_29/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_29	RestoreV2
save/Constsave/RestoreV2_29/tensor_names"save/RestoreV2_29/shape_and_slices*
_output_shapes
:*
dtypes
2
Ą
save/Assign_29AssignFC2/b/Adam_1save/RestoreV2_29*
use_locking(*
T0*
_class

loc:@FC2/b*
validate_shape(*
_output_shapes
:

r
save/RestoreV2_30/tensor_namesConst* 
valueBBbeta1_power*
dtype0*
_output_shapes
:
k
"save/RestoreV2_30/shape_and_slicesConst*
valueB
B *
dtype0*
_output_shapes
:

save/RestoreV2_30	RestoreV2
save/Constsave/RestoreV2_30/tensor_names"save/RestoreV2_30/shape_and_slices*
_output_shapes
:*
dtypes
2

save/Assign_30Assignbeta1_powersave/RestoreV2_30*
validate_shape(*
_output_shapes
: *
use_locking(*
T0*
_class
loc:@Conv1/W
r
save/RestoreV2_31/tensor_namesConst* 
valueBBbeta2_power*
dtype0*
_output_shapes
:
k
"save/RestoreV2_31/shape_and_slicesConst*
_output_shapes
:*
valueB
B *
dtype0

save/RestoreV2_31	RestoreV2
save/Constsave/RestoreV2_31/tensor_names"save/RestoreV2_31/shape_and_slices*
_output_shapes
:*
dtypes
2

save/Assign_31Assignbeta2_powersave/RestoreV2_31*
_class
loc:@Conv1/W*
validate_shape(*
_output_shapes
: *
use_locking(*
T0
Ž
save/restore_shardNoOp^save/Assign^save/Assign_1^save/Assign_2^save/Assign_3^save/Assign_4^save/Assign_5^save/Assign_6^save/Assign_7^save/Assign_8^save/Assign_9^save/Assign_10^save/Assign_11^save/Assign_12^save/Assign_13^save/Assign_14^save/Assign_15^save/Assign_16^save/Assign_17^save/Assign_18^save/Assign_19^save/Assign_20^save/Assign_21^save/Assign_22^save/Assign_23^save/Assign_24^save/Assign_25^save/Assign_26^save/Assign_27^save/Assign_28^save/Assign_29^save/Assign_30^save/Assign_31
-
save/restore_allNoOp^save/restore_shard"<
save/Const:0save/Identity:0save/restore_all (5 @F8"¸
	summariesŞ
§
Conv1/weights:0
Conv1/biases:0
Conv2/weights:0
Conv2/biases:0
Conv3/weights:0
Conv3/biases:0
Cost/per_epoch_per_minibatch:0
"accuracy/per_epoch_per_minibatch:0"Ĺ
trainable_variables­Ş
+
	Conv1/W:0Conv1/W/AssignConv1/W/read:0
+
	Conv1/b:0Conv1/b/AssignConv1/b/read:0
+
	Conv2/W:0Conv2/W/AssignConv2/W/read:0
+
	Conv2/b:0Conv2/b/AssignConv2/b/read:0
+
	Conv3/W:0Conv3/W/AssignConv3/W/read:0
+
	Conv3/b:0Conv3/b/AssignConv3/b/read:0
%
FC1/W:0FC1/W/AssignFC1/W/read:0
%
FC1/b:0FC1/b/AssignFC1/b/read:0
%
FC2/W:0FC2/W/AssignFC2/W/read:0
%
FC2/b:0FC2/b/AssignFC2/b/read:0"
train_op

Adam"é
	variablesŰŘ
+
	Conv1/W:0Conv1/W/AssignConv1/W/read:0
+
	Conv1/b:0Conv1/b/AssignConv1/b/read:0
+
	Conv2/W:0Conv2/W/AssignConv2/W/read:0
+
	Conv2/b:0Conv2/b/AssignConv2/b/read:0
+
	Conv3/W:0Conv3/W/AssignConv3/W/read:0
+
	Conv3/b:0Conv3/b/AssignConv3/b/read:0
%
FC1/W:0FC1/W/AssignFC1/W/read:0
%
FC1/b:0FC1/b/AssignFC1/b/read:0
%
FC2/W:0FC2/W/AssignFC2/W/read:0
%
FC2/b:0FC2/b/AssignFC2/b/read:0
7
beta1_power:0beta1_power/Assignbeta1_power/read:0
7
beta2_power:0beta2_power/Assignbeta2_power/read:0
:
Conv1/W/Adam:0Conv1/W/Adam/AssignConv1/W/Adam/read:0
@
Conv1/W/Adam_1:0Conv1/W/Adam_1/AssignConv1/W/Adam_1/read:0
:
Conv1/b/Adam:0Conv1/b/Adam/AssignConv1/b/Adam/read:0
@
Conv1/b/Adam_1:0Conv1/b/Adam_1/AssignConv1/b/Adam_1/read:0
:
Conv2/W/Adam:0Conv2/W/Adam/AssignConv2/W/Adam/read:0
@
Conv2/W/Adam_1:0Conv2/W/Adam_1/AssignConv2/W/Adam_1/read:0
:
Conv2/b/Adam:0Conv2/b/Adam/AssignConv2/b/Adam/read:0
@
Conv2/b/Adam_1:0Conv2/b/Adam_1/AssignConv2/b/Adam_1/read:0
:
Conv3/W/Adam:0Conv3/W/Adam/AssignConv3/W/Adam/read:0
@
Conv3/W/Adam_1:0Conv3/W/Adam_1/AssignConv3/W/Adam_1/read:0
:
Conv3/b/Adam:0Conv3/b/Adam/AssignConv3/b/Adam/read:0
@
Conv3/b/Adam_1:0Conv3/b/Adam_1/AssignConv3/b/Adam_1/read:0
4
FC1/W/Adam:0FC1/W/Adam/AssignFC1/W/Adam/read:0
:
FC1/W/Adam_1:0FC1/W/Adam_1/AssignFC1/W/Adam_1/read:0
4
FC1/b/Adam:0FC1/b/Adam/AssignFC1/b/Adam/read:0
:
FC1/b/Adam_1:0FC1/b/Adam_1/AssignFC1/b/Adam_1/read:0
4
FC2/W/Adam:0FC2/W/Adam/AssignFC2/W/Adam/read:0
:
FC2/W/Adam_1:0FC2/W/Adam_1/AssignFC2/W/Adam_1/read:0
4
FC2/b/Adam:0FC2/b/Adam/AssignFC2/b/Adam/read:0
:
FC2/b/Adam_1:0FC2/b/Adam_1/AssignFC2/b/Adam_1/read:0